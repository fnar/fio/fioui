﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

using FIOUI.Web;

namespace FIOUI.Tabs.InventoryTab
{
    public partial class InventoryTab : FIOTab
    {
        public List<InventoryModel> InventoryModels = null;

        public override bool CanUseWithOtherUsers
        {
            get
            {
                return true;
            }
        }

        public override List<string> PermissionTypes
        {
            get
            {
                return new List<string> { "storage", "building" };
            }
        }

        public InventoryTab()
        {
            InitializeComponent();

            inventoryTreeListView.CanExpandGetter = model => ((InventoryModel)model).Children.Count > 0;
            inventoryTreeListView.ChildrenGetter = delegate (object model)
            {
                return ((InventoryModel)model).Children;
            };
        }

        private async void InventoryTab_Load(object sender, EventArgs e)
        {
            await RunOnUIThread(() =>
            {
                Text = $"Inventory - {DataUserName}";
            });
        }

        public override List<byte[]> SaveOLVStates()
        {
            return new List<byte[]> { inventoryTreeListView.SaveState() };
        }

        public override bool RestoreOLVStates(List<byte[]> state)
        {
            if (state?.Count == 1)
            {
                return inventoryTreeListView.RestoreState(state[0]);
            }

            return false;
        }

        public override async Task RefreshDataAsync()
        {
            await RunOnUIThread(() =>
            {
                inventoryTreeListView.ClearObjects();
                inventoryTreeListView.EmptyListMsg = "Loading...";
                inventoryTreeListView.Refresh();
            });

            InventoryModels = new List<InventoryModel>();

            JsonRepresentations.SITESPayload allSites = null;
            using (GetRequest sitesRequest = new GetRequest($"/sites/{DataUserName}", Main.Auth.AuthToken))
            {
                allSites = await sitesRequest.GetResponseAsync<JsonRepresentations.SITESPayload>();
            }

            List<JsonRepresentations.Storage.Rootobject> allStorage = null;
            using ( GetRequest storageRequest = new GetRequest($"/storage/{DataUserName}", Main.Auth.AuthToken))
            {
                allStorage = await storageRequest.GetResponseAsync<List<JsonRepresentations.Storage.Rootobject>>();
            }

            if ( allSites != null && allStorage != null )
            {
                foreach (var site in allSites.Sites)
                {
                    var siteStorage = allStorage.Where(s => s.AddressableId == site.SiteId).FirstOrDefault();
                    if (siteStorage != null)
                    {
                        InventoryModel planetModel = new InventoryModel();

                        planetModel.PlanetId = site.PlanetId;
                        planetModel.PlanetName = site.PlanetName;
                        planetModel.PlanetNaturalId = site.PlanetIdentifier;

                        foreach (var item in siteStorage.StorageItems)
                        {
                            InventoryModel planetInventoryModel = new InventoryModel();

                            planetInventoryModel.MaterialName = item?.MaterialTicker;
                            planetInventoryModel.UnitsOnPlanet = item?.MaterialAmount;

                            planetModel.Children.Add(planetInventoryModel);
                        }

                        InventoryModels.Add(planetModel);
                    }
                }
            }

            await RunOnUIThread(() =>
            {
                inventoryTreeListView.SetObjects(InventoryModels);
                inventoryTreeListView.EmptyListMsg = "No data.";
            });

            await Task.FromResult(0);
        }
    }
}
