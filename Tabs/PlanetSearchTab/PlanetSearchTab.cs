﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using FIOUI.Web;

using Newtonsoft.Json;

namespace FIOUI.Tabs.PlanetSearchTab
{
    public partial class PlanetSearchTab : FIOTab
    {
        private List<PlanetSearchModel> PlanetSearchModels = null;
        private Dictionary<string, bool> ResourceToChecked = null;
        private List<string> SettingsCheckedResources = null;

        private List<JsonRepresentations.Material> allResources = null;

        public PlanetSearchTab()
        {
            InitializeComponent();

            Text = "Planet Searcher";

            allowedPlanetSettingsCheckedListBox.SetItemChecked(0, true);
            materialsObjectListView.EmptyListMsg = "No planet selected.";
        }

        private async void PlanetSearchTab_Load(object sender, EventArgs e)
        {
            await Task.FromResult(0);
        }

        public override List<byte[]> SaveOLVStates()
        {
            return new List<byte[]> { planetSearchResultsObjectListView.SaveState(), materialsObjectListView.SaveState() };
        }

        public override bool RestoreOLVStates(List<byte[]> state)
        {
            if ( state?.Count == 2 )
            {
                bool bRestored = true;
                bRestored &= planetSearchResultsObjectListView.RestoreState(state[0]);
                bRestored &= materialsObjectListView.RestoreState(state[1]);
                return bRestored;
            }

            return false;
        }

        internal class PlanetSearchGeneralState
        {
            public List<string> ResourcesChecked { get; set; }
            public List<string> MustHaveSettingsChecked { get; set; }
            public List<string> AllowedToHaveSettingsChecked { get; set; }
        }

        public override string SaveGeneralState()
        {
            PlanetSearchGeneralState generalState = new PlanetSearchGeneralState();
            generalState.ResourcesChecked = ResourceToChecked.Where(r => r.Value).Select(r => r.Key).ToList();
            generalState.MustHaveSettingsChecked = mustHavePlanetPropertiesCheckedListBox.CheckedItems.OfType<string>().ToList();
            generalState.AllowedToHaveSettingsChecked = allowedPlanetSettingsCheckedListBox.CheckedItems.OfType<string>().ToList();

            return JsonConvert.SerializeObject(generalState); ;
        }

        public override bool RestoreGeneralState(string state)
        {
            if (!String.IsNullOrWhiteSpace(state))
            {
                var generalState = JsonConvert.DeserializeObject<PlanetSearchGeneralState>(state);
                if (generalState != null)
                {
                    if ( generalState.ResourcesChecked != null )
                    {
                        SettingsCheckedResources = generalState.ResourcesChecked;
                    }

                    if ( generalState.MustHaveSettingsChecked != null)
                    {
                        for (int itemIdx = 0; itemIdx < mustHavePlanetPropertiesCheckedListBox.Items.Count; ++itemIdx)
                        {
                            string itemStr = mustHavePlanetPropertiesCheckedListBox.Items[itemIdx] as string;
                            mustHavePlanetPropertiesCheckedListBox.SetItemChecked(itemIdx, generalState.MustHaveSettingsChecked.Contains(itemStr));
                        }
                    }

                    if ( generalState.AllowedToHaveSettingsChecked != null )
                    {
                        for (int itemIdx = 0; itemIdx < allowedPlanetSettingsCheckedListBox.Items.Count; ++itemIdx)
                        {
                            string itemStr = allowedPlanetSettingsCheckedListBox.Items[itemIdx] as string;
                            allowedPlanetSettingsCheckedListBox.SetItemChecked(itemIdx, generalState.AllowedToHaveSettingsChecked.Contains(itemStr));
                        }
                    }
                }
            }
            return true;
        }

        private volatile bool IsPopulatingResources = false;
        private async Task PopulatePlanetResources()
        {
            if ( materialsCheckedListBox.Items.Count == 0)
            {
                IsPopulatingResources = true;
                ResourceToChecked = new Dictionary<string, bool>();

                allResources = await DataCache.GetMinerals();
                allResources = allResources.Union(await DataCache.GetLiquids()).ToList();
                allResources = allResources.Union(await DataCache.GetGasses()).ToList();
                allResources = allResources.Union(await DataCache.GetOres()).ToList();
                List<string> allResourceStrs = allResources.Select(r => r.Ticker).ToList();

                int comboBoxIdx = 0;
                foreach( var resourceStr in allResourceStrs)
                {
                    bool bChecked = false;
                    if (SettingsCheckedResources != null)
                    {
                        bChecked = SettingsCheckedResources.Contains(resourceStr);
                    }

                    ResourceToChecked.Add(resourceStr, bChecked);
                    materialsCheckedListBox.Items.Add(resourceStr);
                    materialsCheckedListBox.SetItemChecked(comboBoxIdx++, bChecked);
                }
                IsPopulatingResources = false;
            }
        }

        private string GetDescriptorStr(double value, double lower, double upper)
        {
            if (value < lower)
            {
               return "Low";
            }
            else if (value > upper)
            {
                return "High";
            }

            return "Normal";
        }

        public override async Task RefreshDataAsync()
        {
            await RunOnUIThread(() =>
            {
                materialsCheckedListBox.Enabled = false;
                mustHavePlanetPropertiesCheckedListBox.Enabled = false;
                allowedPlanetSettingsCheckedListBox.Enabled = false;
                searchButton.Enabled = false;

                materialsObjectListView.ClearObjects();
                materialsObjectListView.EmptyListMsg = "No planet selected.";

                planetSearchResultsObjectListView.ClearObjects();
                planetSearchResultsObjectListView.EmptyListMsg = "Loading...";
                planetSearchResultsObjectListView.Refresh();
            });

            PlanetSearchModels = new List<PlanetSearchModel>();

            await PopulatePlanetResources();

            var search = new JsonRepresentations.JsonPlanetSearch();
            search.Materials = ResourceToChecked.Where(r => r.Value).Select(r => r.Key).ToList();

            search.IncludeRocky = allowedPlanetSettingsCheckedListBox.GetItemChecked(0);
            search.IncludeGaseous = allowedPlanetSettingsCheckedListBox.GetItemChecked(1);
            search.IncludeLowGravity = allowedPlanetSettingsCheckedListBox.GetItemChecked(2);
            search.IncludeHighGravity = allowedPlanetSettingsCheckedListBox.GetItemChecked(3);
            search.IncludeLowPressure = allowedPlanetSettingsCheckedListBox.GetItemChecked(4);
            search.IncludeHighPressure = allowedPlanetSettingsCheckedListBox.GetItemChecked(5);
            search.IncludeLowTemperature = allowedPlanetSettingsCheckedListBox.GetItemChecked(6);
            search.IncludeHighTemperature = allowedPlanetSettingsCheckedListBox.GetItemChecked(7);

            search.MustBeFertile = mustHavePlanetPropertiesCheckedListBox.GetItemChecked(0);
            search.MustHaveLocalMarket = mustHavePlanetPropertiesCheckedListBox.GetItemChecked(1);
            search.MustHaveChamberOfCommerce = mustHavePlanetPropertiesCheckedListBox.GetItemChecked(2);
            search.MustHaveWarehouse = mustHavePlanetPropertiesCheckedListBox.GetItemChecked(3);
            search.MustHaveAdministrationCenter = mustHavePlanetPropertiesCheckedListBox.GetItemChecked(4);

            PostRequest planetSearchRequest = new PostRequest("/planet/search", Main.Auth.AuthToken, JsonConvert.SerializeObject(search));
            List<JsonRepresentations.PlanetData> planetResults = await planetSearchRequest.GetResponseAsync<List<JsonRepresentations.PlanetData>>();
            if (planetResults != null)
            {
                foreach(var planetData in planetResults)
                {
                    PlanetSearchModel model = new PlanetSearchModel();

                    model.NaturalId = planetData.NaturalId;
                    model.Name = planetData.Name;
                    model.Fertility = (float)planetData.Fertility;
                    if ( model.Fertility == -1.0 )
                    {
                        model.Fertility = null;
                    }

                    model.SurfaceType = planetData.Surface ? "Rocky" : "Gaseous";
                    model.Gravity = GetDescriptorStr(planetData.Gravity, 0.25, 2.5);
                    model.Pressure = GetDescriptorStr(planetData.Pressure, 0.25, 2.0);
                    model.Temperature = GetDescriptorStr(planetData.Temperature, -25.0, 75.0);
                    model.HasLocalMarket = planetData.HasLocalMarket;
                    model.HasChamberOfCommerce = planetData.HasChamberOfCommerce;
                    model.HasWarehouse = planetData.HasWarehouse;
                    model.HasAdministrationCenter = planetData.HasAdministrationCenter;

                    model.MaterialSummary = String.Empty;
                    foreach (var resource in planetData.Resources)
                    {
                        PlanetResource pr = new PlanetResource();
                        string MatTicker = allResources.Where(r => r.Id == resource.MaterialId).Select(r => r.Ticker).FirstOrDefault();
                        pr.Material = MatTicker;
                        pr.ResourceType = resource.ResourceType.Substring(0, 1) + resource.ResourceType.Substring(1).ToLower(); // GASEOUS -> Gaseous
                        pr.Concentration = resource.Factor;
                        pr.DailyExtraction = (resource.ResourceType == "GASEOUS") ? resource.Factor * 60.0 : resource.Factor * 70.0;
                        model.Resources.Add(pr);

                        model.MaterialSummary += $"{pr.Material} ({pr.Concentration:N2}), ";
                    }

                    if ( model.MaterialSummary.Length > 2)
                    {
                        model.MaterialSummary = model.MaterialSummary.Substring(0, model.MaterialSummary.Length - 2); // Trim off ", " from the end
                    }

                    PlanetSearchModels.Add(model);
                }
            }

            await RunOnUIThread(() =>
            {
                planetSearchResultsObjectListView.SetObjects(PlanetSearchModels);
                planetSearchResultsObjectListView.EmptyListMsg = "No data.";

                materialsCheckedListBox.Enabled = true;
                mustHavePlanetPropertiesCheckedListBox.Enabled = true;
                allowedPlanetSettingsCheckedListBox.Enabled = true;
                searchButton.Enabled = true;
            });
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            _ = RefreshDataAsync();
        }

        private void materialsCheckedListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (!IsPopulatingResources)
            {
                string itemChanged = (string)materialsCheckedListBox.Items[e.Index];
                bool bChecked = (e.NewValue == CheckState.Checked);
                ResourceToChecked[itemChanged] = bChecked;
            }
        }

        private void planetSearchResultsObjectListView_SelectionChanged(object sender, EventArgs e)
        {
            PlanetSearchModel model = planetSearchResultsObjectListView.SelectedObject as PlanetSearchModel;
            if (model != null )
            {
                materialsObjectListView.EmptyListMsg = "No resources.";
                materialsObjectListView.SetObjects(model.Resources);
            }
            else
            {
                materialsObjectListView.EmptyListMsg = "No planet selected.";
            }
        }

        private void planetSearchResultsObjectListView_FormatCell(object sender, BrightIdeasSoftware.FormatCellEventArgs e)
        {
            int colIdx = e.ColumnIndex;
            PlanetSearchModel model = e.Model as PlanetSearchModel;
            if ( model != null )
            {
                if (colIdx == gravityColumn.Index)
                {
                    if ( model.Gravity == "Low" )
                    {
                        e.SubItem.BackColor = Color.LightBlue;
                    }
                    else if ( model.Gravity == "High")
                    {
                        e.SubItem.BackColor = Color.PaleVioletRed;
                    }
                }
                else if (colIdx == pressureColumn.Index)
                {
                    if (model.Pressure == "Low")
                    {
                        e.SubItem.BackColor = Color.LightBlue;
                    }
                    else if (model.Pressure == "High")
                    {
                        e.SubItem.BackColor = Color.PaleVioletRed;
                    }
                }
                else if (colIdx == temperatureColumn.Index)
                {
                    if (model.Temperature == "Low")
                    {
                        e.SubItem.BackColor = Color.LightBlue;
                    }
                    else if (model.Temperature == "High")
                    {
                        e.SubItem.BackColor = Color.PaleVioletRed;
                    }
                }
            }
        }

        private void materialsObjectListView_FormatCell(object sender, BrightIdeasSoftware.FormatCellEventArgs e)
        {
            int colIdx = e.ColumnIndex;
            PlanetResource model = e.Model as PlanetResource;
            if ( model != null )
            {
                double alpha = model.Concentration;
                int component = (int)(255.0 * alpha);
                int background = (int)(255.0 * (1.0-alpha));

                if ( colIdx == concentrationColumn.Index )
                {
                    e.SubItem.BackColor = Color.FromArgb(background, component + background, background);
                }
                else if ( colIdx == dailyExtractionColumn.Index)
                {
                    e.SubItem.BackColor = Color.FromArgb(component + background, component + background, background);
                }
            }
        }
    }
}
