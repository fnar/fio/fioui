﻿using System;
using System.Collections.Generic;

namespace FIOUI.Tabs.PlanetSearchTab
{
    public class PlanetResource
    {
        public string Material;
        public string ResourceType;
        public double Concentration;
        public double DailyExtraction;
    }

    public class PlanetSearchModel
    {
        public string NaturalId { get; set; }
        public string Name { get; set; }

        public string DisplayName
        {
            get
            {
                if ( Name != NaturalId )
                {
                    return $"{Name} ({NaturalId})";
                }

                return NaturalId;
            }
        }

        public float? Fertility { get; set; }
        public string FertilityDisplay
        {
            get
            {
                if (Fertility != null && Fertility > -1.0f)
                {
                    return String.Format("{0:N2}%", Fertility);
                }

                return String.Empty;
            }
        }

        public string SurfaceType { get; set; }

        public string Gravity { get; set; }
        public string Pressure { get; set; }
        public string Temperature { get; set; }

        public bool? HasLocalMarket { get; set; }
        public bool? HasChamberOfCommerce { get; set; }
        public bool? HasWarehouse { get; set; }
        public bool? HasAdministrationCenter { get; set; }

        public string MaterialSummary { get; set; }

        public List<PlanetResource> Resources { get; set; } = new List<PlanetResource>();
    }
}
