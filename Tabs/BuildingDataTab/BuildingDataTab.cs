﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using FIOUI.Web;

namespace FIOUI.Tabs.BuildingDataTab
{
    public partial class BuildingDataTab : FIOTab
    {
        private List<SiteModel> SiteModels = null;

        public override bool CanUseWithOtherUsers
        {
            get
            {
                return true;
            }
        }

        public override List<string> PermissionTypes
        {
            get
            {
                return new List<string> { "building" };
            }
        }

        public BuildingDataTab()
        {
            InitializeComponent();

            buildingDataTreeListView.CanExpandGetter = model => ((SiteModel)model).Children.Count > 0;
            buildingDataTreeListView.ChildrenGetter = delegate (object model)
            {
                return ((SiteModel)model).Children;
            };
        }

        private async void BuildingDataTab_Load(object sender, EventArgs e)
        {
            await RunOnUIThread(() =>
            {
                Text = $"Building Data - {DataUserName}";
            });
        }

        private List<string> TickersToSkip = new List<string>
        {
            "CM", "HB1", "HB2", "HB3", "HB4", "HB5", "HBB", "HBC", "HBL", "HBM", "STO"
        };

        public override List<byte[]> SaveOLVStates()
        {
            return new List<byte[]> { buildingDataTreeListView.SaveState() };
        }

        public override bool RestoreOLVStates(List<byte[]> state)
        {
            if ( state?.Count == 1)
            {
                return buildingDataTreeListView.RestoreState(state[0]);
            }

            return false;
        }

        public override async Task RefreshDataAsync()
        {
            await RunOnUIThread(() =>
            {
                buildingDataTreeListView.ClearObjects();
                buildingDataTreeListView.EmptyListMsg = "Loading...";
                buildingDataTreeListView.Refresh();
            });

            SiteModels = new List<SiteModel>();

            // Load all our workforce data
            JsonRepresentations.SITESPayload allSites = null;
            using (GetRequest sitesRequest = new GetRequest($"/sites/{DataUserName}", Main.Auth.AuthToken))
            {
                allSites = await sitesRequest.GetResponseAsync<JsonRepresentations.SITESPayload>();
            }
                
            if ( allSites != null)
            {
                foreach (var site in allSites.Sites)
                {
                    SiteModel siteModel = new SiteModel();
                    siteModel.PlanetId = site.PlanetId;
                    siteModel.PlanetNaturalId = site.PlanetIdentifier;
                    siteModel.PlanetName = site.PlanetName;

                    foreach (var building in site.Buildings)
                    {
                        if (TickersToSkip.Contains(building.BuildingTicker))
                        {
                            continue;
                        }

                        SiteModel buildingModel = new SiteModel();

                        buildingModel.BuildingTicker = building.BuildingTicker;
                        buildingModel.Condition = String.Format("{0:N2}", building.Condition * 100.0);

                        SiteModel reclaimModel = new SiteModel { ReclaimOrRepair = "Reclaimables" };
                        foreach (var reclaimable in building.ReclaimableMaterials)
                        {
                            SiteModel reclaimableMatModel = new SiteModel();

                            reclaimableMatModel.MaterialTicker = reclaimable.MaterialTicker;
                            reclaimableMatModel.MaterialAmount = reclaimable.MaterialAmount;

                            reclaimModel.Children.Add(reclaimableMatModel);
                        }
                        if ( reclaimModel.Children.Count > 0 )
                        {
                            buildingModel.Children.Add(reclaimModel);
                        }

                        SiteModel repairModel = new SiteModel { ReclaimOrRepair = "Repair" };
                        foreach (var repair in building.RepairMaterials)
                        {
                            SiteModel repairMatModel = new SiteModel();

                            repairMatModel.MaterialTicker = repair.MaterialTicker;
                            repairMatModel.MaterialAmount = repair.MaterialAmount;

                            repairModel.Children.Add(repairMatModel);
                        }
                        if ( repairModel.Children.Count > 0 )
                        {
                            buildingModel.Children.Add(repairModel);
                        }

                        siteModel.Children.Add(buildingModel);
                    }

                    SiteModels.Add(siteModel);
                }
            }

            await RunOnUIThread(() =>
            {
                buildingDataTreeListView.SetObjects(SiteModels);
                buildingDataTreeListView.EmptyListMsg = "No data.";
            });

            await Task.FromResult(0);
        }
    }
}
