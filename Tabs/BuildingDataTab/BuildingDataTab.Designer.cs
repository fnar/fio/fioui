﻿namespace FIOUI.Tabs.BuildingDataTab
{
    partial class BuildingDataTab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuildingDataTab));
            this.buildingDataTreeListView = new BrightIdeasSoftware.TreeListView();
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn4 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn5 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn6 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            ((System.ComponentModel.ISupportInitialize)(this.buildingDataTreeListView)).BeginInit();
            this.SuspendLayout();
            // 
            // buildingDataTreeListView
            // 
            this.buildingDataTreeListView.AllColumns.Add(this.olvColumn1);
            this.buildingDataTreeListView.AllColumns.Add(this.olvColumn2);
            this.buildingDataTreeListView.AllColumns.Add(this.olvColumn3);
            this.buildingDataTreeListView.AllColumns.Add(this.olvColumn4);
            this.buildingDataTreeListView.AllColumns.Add(this.olvColumn5);
            this.buildingDataTreeListView.AllColumns.Add(this.olvColumn6);
            this.buildingDataTreeListView.AllowColumnReorder = true;
            this.buildingDataTreeListView.AlternateRowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.buildingDataTreeListView.CellEditUseWholeCell = false;
            this.buildingDataTreeListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1,
            this.olvColumn2,
            this.olvColumn3,
            this.olvColumn4,
            this.olvColumn5,
            this.olvColumn6});
            this.buildingDataTreeListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.buildingDataTreeListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buildingDataTreeListView.HideSelection = false;
            this.buildingDataTreeListView.Location = new System.Drawing.Point(0, 0);
            this.buildingDataTreeListView.Name = "buildingDataTreeListView";
            this.buildingDataTreeListView.ShowGroups = false;
            this.buildingDataTreeListView.Size = new System.Drawing.Size(800, 450);
            this.buildingDataTreeListView.TabIndex = 0;
            this.buildingDataTreeListView.UseAlternatingBackColors = true;
            this.buildingDataTreeListView.UseCompatibleStateImageBehavior = false;
            this.buildingDataTreeListView.View = System.Windows.Forms.View.Details;
            this.buildingDataTreeListView.VirtualMode = true;
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "PlanetDisplayName";
            this.olvColumn1.Text = "Planet";
            this.olvColumn1.Width = 150;
            // 
            // olvColumn2
            // 
            this.olvColumn2.AspectName = "BuildingTicker";
            this.olvColumn2.Text = "Building";
            // 
            // olvColumn3
            // 
            this.olvColumn3.AspectName = "Condition";
            this.olvColumn3.AspectToStringFormat = "";
            this.olvColumn3.Text = "Condition";
            this.olvColumn3.Width = 80;
            // 
            // olvColumn4
            // 
            this.olvColumn4.AspectName = "ReclaimOrRepair";
            this.olvColumn4.Text = " ";
            this.olvColumn4.Width = 80;
            // 
            // olvColumn5
            // 
            this.olvColumn5.AspectName = "MaterialTicker";
            this.olvColumn5.Text = "Material";
            // 
            // olvColumn6
            // 
            this.olvColumn6.AspectName = "MaterialAmount";
            this.olvColumn6.Text = "Amount";
            // 
            // BuildingDataTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.buildingDataTreeListView);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BuildingDataTab";
            this.Text = "BuildingDataTab";
            this.Load += new System.EventHandler(this.BuildingDataTab_Load);
            ((System.ComponentModel.ISupportInitialize)(this.buildingDataTreeListView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.TreeListView buildingDataTreeListView;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private BrightIdeasSoftware.OLVColumn olvColumn2;
        private BrightIdeasSoftware.OLVColumn olvColumn3;
        private BrightIdeasSoftware.OLVColumn olvColumn4;
        private BrightIdeasSoftware.OLVColumn olvColumn5;
        private BrightIdeasSoftware.OLVColumn olvColumn6;
    }
}