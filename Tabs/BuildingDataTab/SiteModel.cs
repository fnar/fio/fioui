﻿using System.Collections.Generic;

namespace FIOUI.Tabs.BuildingDataTab
{
    public class SiteModel
    {
        public string PlanetId { get; set; }
        public string PlanetNaturalId { get; set; }
        public string PlanetName { get; set; }

        public string PlanetDisplayName
        {
            get
            {
                return (PlanetNaturalId != PlanetName) ? $"{PlanetName} ({PlanetNaturalId})" : PlanetNaturalId;
            }
        }

        public string BuildingTicker { get; set; }
        public string Condition { get; set; }

        public string ReclaimOrRepair { get; set; }
        public string MaterialTicker { get; set; }
        public int? MaterialAmount { get; set; }

        public List<SiteModel> Children { get; set; } = new List<SiteModel>();
    }
}
