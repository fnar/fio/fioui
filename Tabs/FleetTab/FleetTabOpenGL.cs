﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using FIOUI.JsonRepresentations;

using SharpGL;
using SharpGL.SceneGraph;
using SharpGL.SceneGraph.Cameras;
using SharpGL.SceneGraph.Core;
using SharpGL.SceneGraph.Lighting;
using SharpGL.SceneGraph.Primitives;
using SharpGL.SceneGraph.Quadrics;

namespace FIOUI.Tabs.FleetTab
{
    partial class FleetTab
    {
        // Based loosely on radius values from here: https://www.enchantedlearning.com/subjects/astronomy/stars/startypes.shtml
        private Dictionary<string, float> StarTypeToRadius = new Dictionary<string, float>
        {
            { "O", 12.0f },
            { "B", 9.00f },
            { "A", 7.50f },
            { "F", 6.25f },
            { "G", 5.00f },
            { "K", 4.00f },
            { "M", 3.00f }
        };


        private Dictionary<string, SystemStars> SystemIdToSystemStar = new Dictionary<string, SystemStars>();
        private Dictionary<Sphere, SystemStars> SphereToSystemStar = new Dictionary<Sphere, SystemStars>();

        private Dictionary<string, Polygon> SubSectorIdToTriangle = new Dictionary<string, Polygon>();
        private Dictionary<Polygon, Tuple<string, string>> LineToSystemIds = new Dictionary<Polygon, Tuple<string, string>>();

        private List<WorldSectors> WorldSectors = null;

        SharpGL.SceneGraph.Assets.Material UnselectedSphereMaterial = null;
        SharpGL.SceneGraph.Assets.Material UnselectedWorldSectorMaterial = null;
        SharpGL.SceneGraph.Assets.Material UnselectedLineMaterial = null;

        SharpGL.SceneGraph.Assets.Material SelectedSphereMaterial = null;
        SharpGL.SceneGraph.Assets.Material PastLineMaterial = null;
        SharpGL.SceneGraph.Assets.Material FutureLineMaterial = null;

        private void InitMaterials()
        {
            UnselectedSphereMaterial = new SharpGL.SceneGraph.Assets.Material();
            UnselectedSphereMaterial.Ambient = Color.FromArgb(128, 255, 255, 255);
            UnselectedSphereMaterial.Diffuse = Color.FromArgb(128, 255, 255, 255);
            UnselectedSphereMaterial.Emission = Color.FromArgb(128, 255, 255, 255);

            UnselectedWorldSectorMaterial = new SharpGL.SceneGraph.Assets.Material();
            UnselectedWorldSectorMaterial.Ambient = Color.FromArgb(20, 20, 20, 20);

            UnselectedLineMaterial = new SharpGL.SceneGraph.Assets.Material();
            UnselectedLineMaterial.Ambient = Color.FromArgb(255, 128, 128, 128);
            UnselectedLineMaterial.Diffuse = Color.FromArgb(255, 128, 128, 128);
            UnselectedLineMaterial.Emission = Color.FromArgb(128, 128, 128, 128);

            SelectedSphereMaterial = new SharpGL.SceneGraph.Assets.Material();
            SelectedSphereMaterial.Ambient = Color.FromArgb(255, 255, 255, 255);
            SelectedSphereMaterial.Diffuse = Color.FromArgb(255, 255, 255, 255);
            SelectedSphereMaterial.Emission = Color.FromArgb(128, 255, 255, 255);

            PastLineMaterial = new SharpGL.SceneGraph.Assets.Material();
            PastLineMaterial.Ambient = Color.FromArgb(255, 255, 0, 0);
            PastLineMaterial.Diffuse = Color.FromArgb(255, 255, 0, 0);
            PastLineMaterial.Emission = Color.FromArgb(255, 255, 0, 0);

            FutureLineMaterial = new SharpGL.SceneGraph.Assets.Material();
            FutureLineMaterial.Ambient = Color.FromArgb(255, 0, 255, 0);
            FutureLineMaterial.Diffuse = Color.FromArgb(255, 0, 255, 0);
            FutureLineMaterial.Emission = Color.FromArgb(128, 0, 255, 0);
        }

        private async Task initializeOpenGLInfo()
        {
#if DEBUG
            openGLSceneControl.DrawFPS = true;
#else
            openGLSceneControl.DrawFPS = false;
#endif

            InitMaterials();

            {
                var systemStars = await DataCache.GetSystemStars();
                foreach( var sysStar in systemStars )
                {
                    SystemIdToSystemStar.Add(sysStar.SystemId, sysStar);
                }
            }
            
            WorldSectors = await DataCache.GetWorldSectors();

            await RunOnUIThread(() =>
            {                
                openGLSceneControl.Scene.RenderBoundingVolumes = false;

                openGLSceneControl.Scene.CurrentOpenGLContext.ClearColor(0.086f, 0.086f, 0.086f, 1.0f);
                openGLSceneControl.Scene.CurrentOpenGLContext.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);

                // Kill the Light & grid
                openGLSceneControl.Scene.SceneContainer.RemoveChild(openGLSceneControl.Scene.SceneContainer.Children[0]);
                openGLSceneControl.Scene.SceneContainer.RemoveChild(openGLSceneControl.Scene.SceneContainer.Children[0]);

                LookAtCamera cam = openGLSceneControl.Scene.CurrentCamera as LookAtCamera;
                cam.Position = new Vertex(0.0f, 0.0f, 1500.0f);
                cam.Target = new Vertex(0.0f, 0.0f, 0.0f);
                cam.UpVector = new Vertex(0.0f, 1.0f, 0.0f);
                cam.Near = 5.0;
                cam.Far = 3000.0;
                

                // Draw the stars
                foreach (var systemStar in SystemIdToSystemStar.Values)
                {
                    Sphere sphere = new Sphere();
                    sphere.Slices = 25;
                    sphere.Stacks = 100;
                    sphere.Transformation.TranslateX = (float)systemStar.PositionX;
                    sphere.Transformation.TranslateY = (float)systemStar.PositionY;
                    sphere.Transformation.TranslateZ = (float)systemStar.PositionZ;
                    sphere.Radius = StarTypeToRadius[systemStar.Type];
                    sphere.Material = UnselectedSphereMaterial;

                    SphereToSystemStar.Add(sphere, systemStar);

                    openGLSceneControl.Scene.SceneContainer.AddChild(sphere);
                }

                // Draw the WorldSectors
                openGLSceneControl.Scene.CurrentOpenGLContext.PushAttrib(OpenGL.GL_CURRENT_BIT | OpenGL.GL_ENABLE_BIT | OpenGL.GL_LINE_BIT);
                openGLSceneControl.Scene.CurrentOpenGLContext.PolygonMode(SharpGL.Enumerations.FaceMode.FrontAndBack, SharpGL.Enumerations.PolygonMode.Lines);
                foreach ( var worldSector in WorldSectors )
                {
                    foreach(var subSector in worldSector.SubSectors)
                    {
                        Polygon poly = new Polygon();
                        List<Vertex> vertices = new List<Vertex>();
                        foreach( var subSectorVertices in subSector.Vertices )
                        {
                            vertices.Add(new Vertex((float)subSectorVertices.X, (float)subSectorVertices.Y, (float)subSectorVertices.Z));
                        }
                        poly.AddFaceFromVertexData(vertices.ToArray());
                        poly.Material = UnselectedWorldSectorMaterial;
                        SubSectorIdToTriangle.Add(subSector.Id, poly);
                        openGLSceneControl.Scene.SceneContainer.AddChild(poly);
                    }
                }

                // Draw the lines
                openGLSceneControl.Scene.CurrentOpenGLContext.LineWidth(2.0f);
                foreach (var systemStar in SystemIdToSystemStar.Values)
                {
                    Vertex startPos = new Vertex((float)systemStar.PositionX, (float)systemStar.PositionY, (float)systemStar.PositionZ);

                    foreach (var connection in systemStar.Connections)
                    {
                        var existingLine = LineToSystemIds.Values.Where(s => s.Item1 == connection.Connection && s.Item2 == systemStar.SystemId).FirstOrDefault();
                        if (existingLine != null )
                        {
                            // Don't want the reverse direction
                            continue;
                        }

                        var otherStar = SystemIdToSystemStar[connection.Connection];
                        Vertex endPos = new Vertex((float)otherStar.PositionX, (float)otherStar.PositionY, (float)otherStar.PositionZ);

                        Polygon poly = new Polygon();
                        poly.AddFaceFromVertexData(new Vertex[]
                        {
                            new Vertex(startPos.X, startPos.Y, startPos.Z),
                            new Vertex(endPos.X, endPos.Y, endPos.Z),
                        });
                        poly.Material = UnselectedLineMaterial;

                        LineToSystemIds.Add(poly, new Tuple<string, string>(systemStar.SystemId, connection.Connection));
                        openGLSceneControl.Scene.SceneContainer.AddChild(poly);
                    }
                }

                openGLSceneControl.Scene.CurrentOpenGLContext.PopAttrib();
            });
        }

        private List<MouseButtons> MouseButtonsDown = new List<MouseButtons>();
        private void openGLSceneControl_MouseDown(object sender, MouseEventArgs e)
        {
            MouseButtonsDown.Add(e.Button);
        }

        private void openGLSceneControl_MouseUp(object sender, MouseEventArgs e)
        {
            MouseButtonsDown.Remove(e.Button);
        }

        private List<SceneElement> elementsChanged = new List<SceneElement>();

        private int prevX = 0;
        private int prevY = 0;
        private void openGLSceneControl_MouseMove(object sender, MouseEventArgs e)
        {
            // Reset stars
            foreach (var elementChanged in elementsChanged)
            {
                Quadric q = elementChanged as Quadric;
                if (q != null)
                {
                    q.Material = UnselectedSphereMaterial;
                }
                else
                {
                    Polygon p = elementChanged as Polygon;
                    if (p != null)
                    {
                        p.Material = UnselectedLineMaterial;
                    }
                }
            }
            elementsChanged.Clear();

            float deltaX = e.X - prevX;
            float deltaY = e.Y - prevY;
            prevX = e.X;
            prevY = e.Y;

            const float sensitivity = 1.0f;
            deltaX *= sensitivity;
            deltaY *= sensitivity;

            bool bLMBDown = MouseButtonsDown.Contains(MouseButtons.Left);
            bool bRMBDown = MouseButtonsDown.Contains(MouseButtons.Right);
            bool bMMBDown = MouseButtonsDown.Contains(MouseButtons.Middle);

            bool bTraversingXY = (bLMBDown && !bRMBDown);
            bool bRotating = (bRMBDown && !bLMBDown);

            LookAtCamera cam = openGLSceneControl.Scene.CurrentCamera as LookAtCamera;
            if ( bTraversingXY)
            {
                cam.Position = new Vertex(cam.Position.X - deltaX, cam.Position.Y + deltaY, cam.Position.Z);
                cam.Target = new Vertex(cam.Target.X - deltaX, cam.Target.Y + deltaY, cam.Target.Z);
            }
            else if (bRotating)
            {
                //
            }
            else
            {
                //// Hit test: For reference
                //var itemsHit = openGLSceneControl.Scene.DoHitTest(e.X, e.Y);
                //foreach (var item in itemsHit)
                //{
                //    Sphere star = item as Sphere;
                //    if (star != null && SphereToSystemStar.ContainsKey(star))
                //    {
                //        star.Material = SelectedSphereMaterial;
                //        elementsChanged.Add(star);
                //
                //        // TODO: Draw line to Z: 0.  Reset these values.  Maybe List<SceneElement> and cast in the clear portion to Polygon/Quadric?
                //        var sysStar = SphereToSystemStar[star];
                //        var subsectorTri = SubSectorIdToTriangle[sysStar.SubSectorId];
                //        subsectorTri.Material = SelectedSphereMaterial;
                //        elementsChanged.Add(subsectorTri);
                //    }
                //}
            }
        }

        void openGLSceneControl_MouseWheel(object sender, MouseEventArgs e)
        {
            LookAtCamera cam = openGLSceneControl.Scene.CurrentCamera as LookAtCamera;
            cam.Position = new Vertex(cam.Position.X, cam.Position.Y, cam.Position.Z - (e.Delta * 0.5f));
            if ( cam.Position.Z < 200.0f )
            {
                cam.Position = new Vertex(cam.Position.X, cam.Position.Y, 200.0f);
            }
        }

        private List<Polygon> ChangedLines = new List<Polygon>();
        private void NotifyFleetModelChanged()
        {
            // Reset changed lies
            foreach(var line in ChangedLines)
            {
                line.Material = UnselectedLineMaterial;
            }
            ChangedLines.Clear();

            if ( SelectedFleetModel != null )
            {
                int currentSegmentIdx = SelectedFleetModel.CurrentSegmentIndex;
                for (int segIdx = 0; segIdx < SelectedFleetModel.Segments.Count; ++segIdx)
                {
                    var segment = SelectedFleetModel.Segments[segIdx];
                    string start = segment.OriginLines.Where(ol => ol.Type.ToUpper() == "SYSTEM").Select(ol => ol.LineId).FirstOrDefault();
                    string end = segment.DestinationLines.Where(dl => dl.Type.ToUpper() == "SYSTEM").Select(dl => dl.LineId).FirstOrDefault();
                    foreach (var kvp in LineToSystemIds)
                    {
                        if ( (kvp.Value.Item1 == start && kvp.Value.Item2 == end) || (kvp.Value.Item1 == end && kvp.Value.Item2 == start))
                        {
                            // Found
                            Polygon changedLine = kvp.Key;
                            changedLine.Material = segIdx <= currentSegmentIdx ? PastLineMaterial : FutureLineMaterial;
                            ChangedLines.Add(kvp.Key);
                            break;
                        }
                       
                    }
                }
            }
            
        }
    }

}
