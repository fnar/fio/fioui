﻿using System;
using System.Collections.Generic;

namespace FIOUI.Tabs.FleetTab
{
    public class Line
    {
        public string Type { get; set; }
        public string LineId { get; set; }
        public string LineName { get; set; }
        public string LineNaturalId { get; set; }
    }

    public class Segment
    {
        public bool IsSegmentDone { get; set; } = false;
        public bool IsSegmentCurrent { get; set; } = false;

        public string Type { get; set; }
        public string Origin { get; set; }
        public DateTime DepartureTime { get; set; }
        public string Destination { get; set; }
        public DateTime ArrivalTime { get; set; }

        public double? StlDistance { get; set; }
        public double? StlFuelConsumption { get; set; }
        public double? FtlDistance { get; set; }
        public double? FtlFuelConsumption { get; set; }

        public List<Line> OriginLines { get; set; } = new List<Line>();
        public List<Line> DestinationLines { get; set; } = new List<Line>();
    }

    public class FleetModel
    {
        public string UserName { get; set; }

        public string ShipRegistration { get; set; }
        public string ShipName { get; set; }
        public string ShipDisplayName
        {
            get
            {
                return !string.IsNullOrWhiteSpace(ShipName) ? ShipName : ShipRegistration;
            }
        }

        public string Location { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public DateTime? ETA { get; set; }

        public double? SF { get; set; }
        public double? SFMax { get; set; }
        public string SFDisplay
        {
            get
            {
                if (SF != null && SFMax != null)
                {
                    int iSF = (int)SF;
                    int iSFMax = (int)SFMax;
                    return $"{iSF}/{iSFMax}";
                }

                return String.Empty;
            }
        }

        public double? FF { get; set; }
        public double? FFMax { get; set; }
        public string FFDisplay
        {
            get
            {
                if (FF != null && FFMax != null)
                {
                    int iFF = (int)FF;
                    int iFFMax = (int)FFMax;
                    return $"{iFF}/{iFFMax}";
                }

                return String.Empty;
            }
        }

        public DateTime UpdateTime;

        public int CurrentSegmentIndex { get; set; } = -1;

        public List<Segment> Segments { get; set; } = new List<Segment>();
    }
}
