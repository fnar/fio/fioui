﻿
namespace FIOUI.Tabs.FleetTab
{
    partial class FleetTab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.usersCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.segmentsObjectListView = new BrightIdeasSoftware.ObjectListView();
            this.segmentTypeColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.segmentOriginColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.segmentDepartureTimeColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.segmentDestinationColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.segmentArrivalTimeColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.segmentStlDistanceColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.segmentStlFuelUsageColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.segmentFtlDistanceColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.segmentFtlFuelConsumptionColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.refreshButton = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.flightsObjectListView = new BrightIdeasSoftware.ObjectListView();
            this.userNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.shipColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.locationColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.originColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.destinationColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.etaColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.sfColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.ffColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.updateTimeColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.openGLSceneControl = new SharpGL.SceneControl();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.segmentsObjectListView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flightsObjectListView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.openGLSceneControl)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.splitContainer1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 171F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1147, 450);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 91F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.segmentsObjectListView, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.refreshButton, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1141, 165);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.usersCheckedListBox);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(164, 159);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Users:";
            // 
            // usersCheckedListBox
            // 
            this.usersCheckedListBox.CheckOnClick = true;
            this.usersCheckedListBox.FormattingEnabled = true;
            this.usersCheckedListBox.Location = new System.Drawing.Point(3, 16);
            this.usersCheckedListBox.Name = "usersCheckedListBox";
            this.usersCheckedListBox.Size = new System.Drawing.Size(155, 139);
            this.usersCheckedListBox.TabIndex = 1;
            // 
            // segmentsObjectListView
            // 
            this.segmentsObjectListView.AllColumns.Add(this.segmentTypeColumn);
            this.segmentsObjectListView.AllColumns.Add(this.segmentOriginColumn);
            this.segmentsObjectListView.AllColumns.Add(this.segmentDepartureTimeColumn);
            this.segmentsObjectListView.AllColumns.Add(this.segmentDestinationColumn);
            this.segmentsObjectListView.AllColumns.Add(this.segmentArrivalTimeColumn);
            this.segmentsObjectListView.AllColumns.Add(this.segmentStlDistanceColumn);
            this.segmentsObjectListView.AllColumns.Add(this.segmentStlFuelUsageColumn);
            this.segmentsObjectListView.AllColumns.Add(this.segmentFtlDistanceColumn);
            this.segmentsObjectListView.AllColumns.Add(this.segmentFtlFuelConsumptionColumn);
            this.segmentsObjectListView.AlternateRowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.segmentsObjectListView.CellEditUseWholeCell = false;
            this.segmentsObjectListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.segmentTypeColumn,
            this.segmentOriginColumn,
            this.segmentDepartureTimeColumn,
            this.segmentDestinationColumn,
            this.segmentArrivalTimeColumn,
            this.segmentStlDistanceColumn,
            this.segmentStlFuelUsageColumn,
            this.segmentFtlDistanceColumn,
            this.segmentFtlFuelConsumptionColumn});
            this.segmentsObjectListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.segmentsObjectListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.segmentsObjectListView.FullRowSelect = true;
            this.segmentsObjectListView.HideSelection = false;
            this.segmentsObjectListView.Location = new System.Drawing.Point(264, 3);
            this.segmentsObjectListView.Name = "segmentsObjectListView";
            this.segmentsObjectListView.ShowGroups = false;
            this.segmentsObjectListView.Size = new System.Drawing.Size(874, 159);
            this.segmentsObjectListView.TabIndex = 1;
            this.segmentsObjectListView.UseAlternatingBackColors = true;
            this.segmentsObjectListView.UseCompatibleStateImageBehavior = false;
            this.segmentsObjectListView.View = System.Windows.Forms.View.Details;
            this.segmentsObjectListView.FormatRow += new System.EventHandler<BrightIdeasSoftware.FormatRowEventArgs>(this.segmentsObjectListView_FormatRow);
            // 
            // segmentTypeColumn
            // 
            this.segmentTypeColumn.AspectName = "Type";
            this.segmentTypeColumn.Text = "Type";
            this.segmentTypeColumn.Width = 77;
            // 
            // segmentOriginColumn
            // 
            this.segmentOriginColumn.AspectName = "Origin";
            this.segmentOriginColumn.Text = "Origin";
            this.segmentOriginColumn.Width = 79;
            // 
            // segmentDepartureTimeColumn
            // 
            this.segmentDepartureTimeColumn.AspectName = "DepartureTime";
            this.segmentDepartureTimeColumn.Text = "Departure Time";
            this.segmentDepartureTimeColumn.Width = 96;
            // 
            // segmentDestinationColumn
            // 
            this.segmentDestinationColumn.AspectName = "Destination";
            this.segmentDestinationColumn.Text = "Destination";
            this.segmentDestinationColumn.Width = 94;
            // 
            // segmentArrivalTimeColumn
            // 
            this.segmentArrivalTimeColumn.AspectName = "ArrivalTime";
            this.segmentArrivalTimeColumn.Text = "Arrival Time";
            this.segmentArrivalTimeColumn.Width = 80;
            // 
            // segmentStlDistanceColumn
            // 
            this.segmentStlDistanceColumn.AspectName = "StlDistance";
            this.segmentStlDistanceColumn.AspectToStringFormat = "{0:N2}";
            this.segmentStlDistanceColumn.Text = "STL Distance";
            this.segmentStlDistanceColumn.Width = 88;
            // 
            // segmentStlFuelUsageColumn
            // 
            this.segmentStlFuelUsageColumn.AspectName = "StlFuelConsumption";
            this.segmentStlFuelUsageColumn.AspectToStringFormat = "{0:N2}";
            this.segmentStlFuelUsageColumn.Text = "SF Usage";
            this.segmentStlFuelUsageColumn.Width = 67;
            // 
            // segmentFtlDistanceColumn
            // 
            this.segmentFtlDistanceColumn.AspectName = "FtlDistance";
            this.segmentFtlDistanceColumn.AspectToStringFormat = "{0:N2}";
            this.segmentFtlDistanceColumn.Text = "FTL Distance";
            this.segmentFtlDistanceColumn.Width = 81;
            // 
            // segmentFtlFuelConsumptionColumn
            // 
            this.segmentFtlFuelConsumptionColumn.AspectName = "FtlFuelConsumption";
            this.segmentFtlFuelConsumptionColumn.AspectToStringFormat = "{0:N2}";
            this.segmentFtlFuelConsumptionColumn.Text = "FF Usage";
            this.segmentFtlFuelConsumptionColumn.Width = 68;
            // 
            // refreshButton
            // 
            this.refreshButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.refreshButton.Location = new System.Drawing.Point(178, 127);
            this.refreshButton.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(75, 23);
            this.refreshButton.TabIndex = 2;
            this.refreshButton.Text = "Refresh";
            this.refreshButton.UseVisualStyleBackColor = true;
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 174);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.flightsObjectListView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.openGLSceneControl);
            this.splitContainer1.Size = new System.Drawing.Size(1141, 273);
            this.splitContainer1.SplitterDistance = 725;
            this.splitContainer1.TabIndex = 2;
            // 
            // flightsObjectListView
            // 
            this.flightsObjectListView.AllColumns.Add(this.userNameColumn);
            this.flightsObjectListView.AllColumns.Add(this.shipColumn);
            this.flightsObjectListView.AllColumns.Add(this.locationColumn);
            this.flightsObjectListView.AllColumns.Add(this.originColumn);
            this.flightsObjectListView.AllColumns.Add(this.destinationColumn);
            this.flightsObjectListView.AllColumns.Add(this.etaColumn);
            this.flightsObjectListView.AllColumns.Add(this.sfColumn);
            this.flightsObjectListView.AllColumns.Add(this.ffColumn);
            this.flightsObjectListView.AllColumns.Add(this.updateTimeColumn);
            this.flightsObjectListView.AlternateRowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.flightsObjectListView.CellEditUseWholeCell = false;
            this.flightsObjectListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.userNameColumn,
            this.shipColumn,
            this.locationColumn,
            this.originColumn,
            this.destinationColumn,
            this.etaColumn,
            this.sfColumn,
            this.ffColumn,
            this.updateTimeColumn});
            this.flightsObjectListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.flightsObjectListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flightsObjectListView.FullRowSelect = true;
            this.flightsObjectListView.HideSelection = false;
            this.flightsObjectListView.Location = new System.Drawing.Point(0, 0);
            this.flightsObjectListView.MultiSelect = false;
            this.flightsObjectListView.Name = "flightsObjectListView";
            this.flightsObjectListView.ShowGroups = false;
            this.flightsObjectListView.Size = new System.Drawing.Size(725, 273);
            this.flightsObjectListView.TabIndex = 0;
            this.flightsObjectListView.UseAlternatingBackColors = true;
            this.flightsObjectListView.UseCompatibleStateImageBehavior = false;
            this.flightsObjectListView.View = System.Windows.Forms.View.Details;
            this.flightsObjectListView.SelectionChanged += new System.EventHandler(this.flightsObjectListView_SelectionChanged);
            // 
            // userNameColumn
            // 
            this.userNameColumn.AspectName = "UserName";
            this.userNameColumn.Text = "User";
            this.userNameColumn.Width = 99;
            // 
            // shipColumn
            // 
            this.shipColumn.AspectName = "ShipDisplayName";
            this.shipColumn.Text = "Ship";
            this.shipColumn.Width = 83;
            // 
            // locationColumn
            // 
            this.locationColumn.AspectName = "Location";
            this.locationColumn.Text = "Location";
            this.locationColumn.Width = 93;
            // 
            // originColumn
            // 
            this.originColumn.AspectName = "Origin";
            this.originColumn.Text = "Origin";
            this.originColumn.Width = 96;
            // 
            // destinationColumn
            // 
            this.destinationColumn.AspectName = "Destination";
            this.destinationColumn.Text = "Destination";
            this.destinationColumn.Width = 100;
            // 
            // etaColumn
            // 
            this.etaColumn.AspectName = "ETA";
            this.etaColumn.Text = "ETA";
            // 
            // sfColumn
            // 
            this.sfColumn.AspectName = "SFDisplay";
            this.sfColumn.Text = "SF";
            this.sfColumn.Width = 77;
            // 
            // ffColumn
            // 
            this.ffColumn.AspectName = "FFDisplay";
            this.ffColumn.Text = "FF";
            this.ffColumn.Width = 71;
            // 
            // updateTimeColumn
            // 
            this.updateTimeColumn.AspectName = "UpdateTime";
            this.updateTimeColumn.Text = "Last Updated";
            this.updateTimeColumn.Width = 122;
            // 
            // openGLSceneControl
            // 
            this.openGLSceneControl.Cursor = System.Windows.Forms.Cursors.Default;
            this.openGLSceneControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.openGLSceneControl.DrawFPS = false;
            this.openGLSceneControl.Location = new System.Drawing.Point(0, 0);
            this.openGLSceneControl.Name = "openGLSceneControl";
            this.openGLSceneControl.OpenGLVersion = SharpGL.Version.OpenGLVersion.OpenGL2_1;
            this.openGLSceneControl.RenderContextType = SharpGL.RenderContextType.FBO;
            this.openGLSceneControl.RenderTrigger = SharpGL.RenderTrigger.TimerBased;
            this.openGLSceneControl.Size = new System.Drawing.Size(412, 273);
            this.openGLSceneControl.TabIndex = 0;
            this.openGLSceneControl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.openGLSceneControl_MouseDown);
            this.openGLSceneControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.openGLSceneControl_MouseMove);
            this.openGLSceneControl.MouseUp += new System.Windows.Forms.MouseEventHandler(this.openGLSceneControl_MouseUp);
            // 
            // FleetTab
            // 
            this.AcceptButton = this.refreshButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1147, 450);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "FleetTab";
            this.Text = "FleetTab";
            this.Load += new System.EventHandler(this.FleetTab_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.segmentsObjectListView)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flightsObjectListView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.openGLSceneControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private BrightIdeasSoftware.ObjectListView flightsObjectListView;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox usersCheckedListBox;
        private BrightIdeasSoftware.ObjectListView segmentsObjectListView;
        private BrightIdeasSoftware.OLVColumn segmentTypeColumn;
        private BrightIdeasSoftware.OLVColumn segmentOriginColumn;
        private BrightIdeasSoftware.OLVColumn segmentDepartureTimeColumn;
        private BrightIdeasSoftware.OLVColumn segmentDestinationColumn;
        private BrightIdeasSoftware.OLVColumn segmentArrivalTimeColumn;
        private BrightIdeasSoftware.OLVColumn segmentStlDistanceColumn;
        private BrightIdeasSoftware.OLVColumn segmentStlFuelUsageColumn;
        private BrightIdeasSoftware.OLVColumn segmentFtlDistanceColumn;
        private BrightIdeasSoftware.OLVColumn segmentFtlFuelConsumptionColumn;
        private BrightIdeasSoftware.OLVColumn userNameColumn;
        private BrightIdeasSoftware.OLVColumn shipColumn;
        private BrightIdeasSoftware.OLVColumn locationColumn;
        private BrightIdeasSoftware.OLVColumn originColumn;
        private BrightIdeasSoftware.OLVColumn destinationColumn;
        private BrightIdeasSoftware.OLVColumn etaColumn;
        private BrightIdeasSoftware.OLVColumn sfColumn;
        private BrightIdeasSoftware.OLVColumn ffColumn;
        private BrightIdeasSoftware.OLVColumn updateTimeColumn;
        private System.Windows.Forms.Button refreshButton;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private SharpGL.SceneControl openGLSceneControl;
    }
}