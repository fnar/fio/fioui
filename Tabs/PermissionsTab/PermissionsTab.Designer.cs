﻿namespace FIOUI.Tabs.PermissionsTab
{
    partial class PermissionsTab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PermissionsTab));
            this.permissionsListView = new BrightIdeasSoftware.ObjectListView();
            this.UserNameColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.flightDataColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.buildingDataColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.storageDataColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.productionDataColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.workforceDataColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.expertsDataColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.applyChangesButton = new System.Windows.Forms.Button();
            this.addPermissionButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.removePermissionButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.permissionsListView)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // permissionsListView
            // 
            this.permissionsListView.AllColumns.Add(this.UserNameColumn);
            this.permissionsListView.AllColumns.Add(this.flightDataColumn);
            this.permissionsListView.AllColumns.Add(this.buildingDataColumn);
            this.permissionsListView.AllColumns.Add(this.storageDataColumn);
            this.permissionsListView.AllColumns.Add(this.productionDataColumn);
            this.permissionsListView.AllColumns.Add(this.workforceDataColumn);
            this.permissionsListView.AllColumns.Add(this.expertsDataColumn);
            this.permissionsListView.AllowColumnReorder = true;
            this.permissionsListView.AlternateRowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.permissionsListView.CellEditActivation = BrightIdeasSoftware.ObjectListView.CellEditActivateMode.DoubleClick;
            this.permissionsListView.CellEditUseWholeCell = false;
            this.permissionsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.UserNameColumn,
            this.flightDataColumn,
            this.buildingDataColumn,
            this.storageDataColumn,
            this.productionDataColumn,
            this.workforceDataColumn,
            this.expertsDataColumn});
            this.permissionsListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.permissionsListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.permissionsListView.HideSelection = false;
            this.permissionsListView.Location = new System.Drawing.Point(3, 3);
            this.permissionsListView.Name = "permissionsListView";
            this.permissionsListView.ShowGroups = false;
            this.permissionsListView.Size = new System.Drawing.Size(1107, 557);
            this.permissionsListView.TabIndex = 0;
            this.permissionsListView.UseAlternatingBackColors = true;
            this.permissionsListView.UseCompatibleStateImageBehavior = false;
            this.permissionsListView.View = System.Windows.Forms.View.Details;
            this.permissionsListView.SelectedIndexChanged += new System.EventHandler(this.PermissionsTab_SelectedIndexChanged);
            // 
            // UserNameColumn
            // 
            this.UserNameColumn.AspectName = "UserName";
            this.UserNameColumn.Text = "User Name";
            this.UserNameColumn.Width = 150;
            // 
            // flightDataColumn
            // 
            this.flightDataColumn.AspectName = "FlightData";
            this.flightDataColumn.CheckBoxes = true;
            this.flightDataColumn.Text = "Flight Data";
            this.flightDataColumn.Width = 100;
            // 
            // buildingDataColumn
            // 
            this.buildingDataColumn.AspectName = "BuildingData";
            this.buildingDataColumn.CheckBoxes = true;
            this.buildingDataColumn.Text = "Building Data";
            this.buildingDataColumn.Width = 100;
            // 
            // storageDataColumn
            // 
            this.storageDataColumn.AspectName = "StorageData";
            this.storageDataColumn.CheckBoxes = true;
            this.storageDataColumn.Text = "Storage Data";
            this.storageDataColumn.Width = 100;
            // 
            // productionDataColumn
            // 
            this.productionDataColumn.AspectName = "ProductionData";
            this.productionDataColumn.CheckBoxes = true;
            this.productionDataColumn.Text = "Production Data";
            this.productionDataColumn.Width = 100;
            // 
            // workforceDataColumn
            // 
            this.workforceDataColumn.AspectName = "WorkforceData";
            this.workforceDataColumn.CheckBoxes = true;
            this.workforceDataColumn.Text = "Workforce Data";
            this.workforceDataColumn.Width = 100;
            // 
            // expertsDataColumn
            // 
            this.expertsDataColumn.AspectName = "ExpertsData";
            this.expertsDataColumn.CheckBoxes = true;
            this.expertsDataColumn.Text = "Experts Data";
            this.expertsDataColumn.Width = 100;
            // 
            // applyChangesButton
            // 
            this.applyChangesButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.applyChangesButton.Location = new System.Drawing.Point(995, 5);
            this.applyChangesButton.Name = "applyChangesButton";
            this.applyChangesButton.Size = new System.Drawing.Size(109, 36);
            this.applyChangesButton.TabIndex = 1;
            this.applyChangesButton.Text = "Apply Changes";
            this.applyChangesButton.UseVisualStyleBackColor = true;
            this.applyChangesButton.Click += new System.EventHandler(this.applyChangesButton_Click);
            // 
            // addPermissionButton
            // 
            this.addPermissionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.addPermissionButton.Location = new System.Drawing.Point(3, 5);
            this.addPermissionButton.Name = "addPermissionButton";
            this.addPermissionButton.Size = new System.Drawing.Size(98, 36);
            this.addPermissionButton.TabIndex = 2;
            this.addPermissionButton.Text = "Add Permission";
            this.addPermissionButton.UseVisualStyleBackColor = true;
            this.addPermissionButton.Click += new System.EventHandler(this.addPermissionButton_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.permissionsListView, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1113, 613);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 175F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.Controls.Add(this.addPermissionButton, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.applyChangesButton, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.removePermissionButton, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 566);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1107, 44);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // removePermissionButton
            // 
            this.removePermissionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.removePermissionButton.Location = new System.Drawing.Point(128, 5);
            this.removePermissionButton.Name = "removePermissionButton";
            this.removePermissionButton.Size = new System.Drawing.Size(154, 36);
            this.removePermissionButton.TabIndex = 3;
            this.removePermissionButton.Text = "Remove Selected Permission";
            this.removePermissionButton.UseVisualStyleBackColor = true;
            this.removePermissionButton.Click += new System.EventHandler(this.removePermissionButton_Click);
            // 
            // PermissionsTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1113, 613);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PermissionsTab";
            this.Text = "PermissionsTab";
            this.Load += new System.EventHandler(this.PermissionsTab_Load);
            ((System.ComponentModel.ISupportInitialize)(this.permissionsListView)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.ObjectListView permissionsListView;
        private BrightIdeasSoftware.OLVColumn UserNameColumn;
        private System.Windows.Forms.Button applyChangesButton;
        private BrightIdeasSoftware.OLVColumn flightDataColumn;
        private BrightIdeasSoftware.OLVColumn buildingDataColumn;
        private BrightIdeasSoftware.OLVColumn storageDataColumn;
        private BrightIdeasSoftware.OLVColumn productionDataColumn;
        private BrightIdeasSoftware.OLVColumn workforceDataColumn;
        private BrightIdeasSoftware.OLVColumn expertsDataColumn;
        private System.Windows.Forms.Button addPermissionButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button removePermissionButton;
    }
}