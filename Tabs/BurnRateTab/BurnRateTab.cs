﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

using FIOUI.Web;
using Newtonsoft.Json;

namespace FIOUI.Tabs.BurnRateTab
{
    public partial class BurnRateTab : FIOTab
    {
        private List<BurnRateModel> WorkforceModels = null;
        private Dictionary<string, bool> ConsumableToChecked = null;

        private List<string> SettingsCheckedConsumables = null;

        public override bool CanUseWithOtherUsers
        {
            get
            {
                return true;
            }
        }

        public override List<string> PermissionTypes
        {
            get
            {
                return new List<string> { "workforce", "storage" };
            }
        }

        public BurnRateTab()
        {
            InitializeComponent();

            baseOverviewTreeListView.CanExpandGetter = model => ((BurnRateModel)model).Children.Count > 0;
            baseOverviewTreeListView.ChildrenGetter = delegate (object model)
            {
                return ((BurnRateModel)model).Children;
            };
        }

        private void WorkforceTab_Load(object sender, EventArgs e)
        {
            Text = $"Burn Rate - {DataUserName}";
        }

        internal class BurnRateGeneralState
        {
            public int YellowThreshold { get; set; }
            public int RedThreshold { get; set; }
            public List<string> ConsumablesChecked { get; set; }
        }

        public override string SaveGeneralState()
        {
            BurnRateGeneralState generalState = new BurnRateGeneralState();
            generalState.YellowThreshold = (int)yellowThresholdNumericUpDown.Value;
            generalState.RedThreshold = (int)redThresholdNumericUpDown.Value;
            generalState.ConsumablesChecked = ConsumableToChecked.Where(c => c.Value).Select(c => c.Key).ToList();

            return JsonConvert.SerializeObject(generalState);
            
        }

        public override bool RestoreGeneralState(string state)
        {
            if ( !String.IsNullOrWhiteSpace(state))
            {
                var generalState = JsonConvert.DeserializeObject<BurnRateGeneralState>(state);
                if (generalState != null)
                {
                    yellowThresholdNumericUpDown.Value = generalState.YellowThreshold;
                    redThresholdNumericUpDown.Value = generalState.RedThreshold;
                    SettingsCheckedConsumables = generalState.ConsumablesChecked;
                }
            }
            
            return true;
        }

        public override List<byte[]> SaveOLVStates()
        {
            return new List<byte[]> { baseOverviewTreeListView.SaveState() };
        }

        public override bool RestoreOLVStates(List<byte[]> state)
        {
            if ( state?.Count == 1 )
            {
                return baseOverviewTreeListView.RestoreState(state[0]);
            }

            return false;
        }

        private volatile bool IsPopulatingConsumables = false;
        private async Task PopulateConsumablesDropdown()
        {
            if ( consumablesToShowCheckedListBox.Items.Count == 0)
            {
                IsPopulatingConsumables = true;
                ConsumableToChecked = new Dictionary<string, bool>();

                List<JsonRepresentations.Material> allConsumables = await DataCache.GetBasicConsumables();
                allConsumables = allConsumables.Union(await DataCache.GetLuxuryConsumables()).ToList();
                List<string> consumableStrs = allConsumables.Select(c => c.Ticker).ToList();

                int comboBoxIdx = 0;
                foreach (var consumableStr in consumableStrs)
                {
                    bool bChecked = true;
                    if (SettingsCheckedConsumables != null)
                    {
                        bChecked = SettingsCheckedConsumables.Contains(consumableStr);
                    }

                    ConsumableToChecked.Add(consumableStr, bChecked);
                    consumablesToShowCheckedListBox.Items.Add(consumableStr);
                    consumablesToShowCheckedListBox.SetItemChecked(comboBoxIdx++, bChecked);
                }
                IsPopulatingConsumables = false;
            }
        }

        public override async Task RefreshDataAsync()
        {
            await RunOnUIThread(() =>
            {
                yellowThresholdNumericUpDown.Enabled = false;
                redThresholdNumericUpDown.Enabled = false;
                consumablesToShowCheckedListBox.Enabled = false;

                baseOverviewTreeListView.ClearObjects();
                baseOverviewTreeListView.EmptyListMsg = "Loading...";
                baseOverviewTreeListView.Refresh();
            });
            
            WorkforceModels = new List<BurnRateModel>();

            await PopulateConsumablesDropdown();

            // Load all our workforce data
            List<JsonRepresentations.Workforce.Rootobject> allWorkforces = null;
            using (GetRequest workforceRequest = new GetRequest($"/workforce/{DataUserName}", Main.Auth.AuthToken))
            {
                allWorkforces = await workforceRequest.GetResponseAsync<List<JsonRepresentations.Workforce.Rootobject>>();
            }

            // Load all our storage data
            List <JsonRepresentations.Storage.Rootobject> allStorage = null;
            using (GetRequest storageRequest = new GetRequest($"/storage/{DataUserName}", Main.Auth.AuthToken))
            {
                allStorage = await storageRequest.GetResponseAsync<List<JsonRepresentations.Storage.Rootobject>>();
            }

            if (allWorkforces != null && allStorage != null )
            {
                foreach (var planetWorkforces in allWorkforces)
                {
                    BurnRateModel planetWM = new BurnRateModel();
                    foreach (var planetWorkforce in planetWorkforces.Workforces)
                    {
                        if (planetWorkforce.Population > 0)
                        {
                            planetWM.PlanetId = planetWorkforces.PlanetId;
                            planetWM.PlanetName = planetWorkforces.PlanetName;
                            planetWM.PlanetNaturalId = planetWorkforces.PlanetNaturalId;
                            
                            foreach (var workforceNeed in planetWorkforce.WorkforceNeeds)
                            {
                                string material = workforceNeed.MaterialTicker;
                                if ( ConsumableToChecked.ContainsKey(material) && ConsumableToChecked[material])
                                {
                                    var wm = planetWM.Children.Where(c => c.Material == material).FirstOrDefault();
                                    bool bFoundExisting = (wm != null);
                                    if (!bFoundExisting)
                                    {
                                        wm = new BurnRateModel();

                                        wm.Material = material;
                                        wm.BurnRate = workforceNeed.UnitsPerInterval;

                                        var planetStorage = allStorage.Where(s => s.AddressableId == planetWorkforces.SiteId).FirstOrDefault();
                                        var storageItem = (planetStorage != null) ? planetStorage.StorageItems.Where(si => si.MaterialTicker == wm.Material).FirstOrDefault() : null;
                                        wm.UnitsOnPlanet = (storageItem != null) ? storageItem.MaterialAmount : 0;
                                        wm.DaysRemaining = (wm.UnitsOnPlanet > 0) ? wm.UnitsOnPlanet / wm.BurnRate : 0;

                                        planetWM.Children.Add(wm);
                                    }
                                    else
                                    {
                                        wm.BurnRate += workforceNeed.UnitsPerInterval;
                                        wm.DaysRemaining = (wm.UnitsOnPlanet > 0) ? wm.UnitsOnPlanet / wm.BurnRate : 0;
                                    }
                                }
                            }
                        }
                    }
                    if (planetWM.PlanetId != null)
                    {
                        WorkforceModels.Add(planetWM);
                    }
                }
            }

            await RunOnUIThread(() =>
            {
                baseOverviewTreeListView.SetObjects(WorkforceModels);
                baseOverviewTreeListView.EmptyListMsg = "No data.";
                baseOverviewTreeListView.ExpandAll();

                yellowThresholdNumericUpDown.Enabled = true;
                redThresholdNumericUpDown.Enabled = true;
                consumablesToShowCheckedListBox.Enabled = true;
            });
        }

        private void baseOverviewTreeListView_FormatCell(object sender, BrightIdeasSoftware.FormatCellEventArgs e)
        {
            if (e.ColumnIndex == daysRemainingColumn.Index)
            {
                BurnRateModel burnRate = (BurnRateModel)e.Model;
                if (burnRate != null && burnRate.DaysRemaining != null && burnRate.BurnRate > 0)
                {
                    if ((float)burnRate.DaysRemaining < (float)yellowThresholdNumericUpDown.Value)
                    {
                        e.SubItem.BackColor = Color.Yellow;
                    }
                    if ((float)burnRate.DaysRemaining < (float)redThresholdNumericUpDown.Value)
                    {
                        e.SubItem.BackColor = Color.Red;
                    }
                }
            }
        }

        private void yellowThresholdNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (yellowThresholdNumericUpDown.Value <= redThresholdNumericUpDown.Value)
            {
                int yellow = (int)yellowThresholdNumericUpDown.Value;
                int red = (int)redThresholdNumericUpDown.Value;

                // Try to push yellow up first
                yellow = red;
                yellow++;
                if (yellow > yellowThresholdNumericUpDown.Maximum)
                {
                    yellow--;
                    red--;
                }

                yellowThresholdNumericUpDown.Value = yellow;
                redThresholdNumericUpDown.Value = red;
            }

            baseOverviewTreeListView.Refresh();
        }

        private void redThresholdNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (yellowThresholdNumericUpDown.Value <= redThresholdNumericUpDown.Value)
            {
                int yellow = (int)yellowThresholdNumericUpDown.Value;
                int red = (int)redThresholdNumericUpDown.Value;

                // Try to push red down first
                red = yellow;
                red--;
                if ( red < redThresholdNumericUpDown.Minimum)
                {
                    red++;
                    yellow++;
                }

                redThresholdNumericUpDown.Value = red;
                yellowThresholdNumericUpDown.Value = yellow;

            }

            baseOverviewTreeListView.Refresh();
        }

        private void consumablesToShowCheckedListBox_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
        {
            if (!IsPopulatingConsumables)
            {
                string itemChanged = (string)consumablesToShowCheckedListBox.Items[e.Index];
                bool bChecked = (e.NewValue == System.Windows.Forms.CheckState.Checked);
                ConsumableToChecked[itemChanged] = bChecked;
                _ = RefreshDataAsync();
            }
        }
    }
}
