﻿namespace FIOUI.Tabs.BurnRateTab
{
    partial class BurnRateTab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BurnRateTab));
            this.baseOverviewTreeListView = new BrightIdeasSoftware.TreeListView();
            this.planetColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.materialColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.burnRateColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.unitsOnPlanetColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.daysRemainingColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.yellowThresholdNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.consumablesToShowCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.redThresholdNumericUpDown = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.baseOverviewTreeListView)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.yellowThresholdNumericUpDown)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.redThresholdNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // baseOverviewTreeListView
            // 
            this.baseOverviewTreeListView.AllColumns.Add(this.planetColumn);
            this.baseOverviewTreeListView.AllColumns.Add(this.materialColumn);
            this.baseOverviewTreeListView.AllColumns.Add(this.burnRateColumn);
            this.baseOverviewTreeListView.AllColumns.Add(this.unitsOnPlanetColumn);
            this.baseOverviewTreeListView.AllColumns.Add(this.daysRemainingColumn);
            this.baseOverviewTreeListView.AllowColumnReorder = true;
            this.baseOverviewTreeListView.AlternateRowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.baseOverviewTreeListView.CellEditUseWholeCell = false;
            this.baseOverviewTreeListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.planetColumn,
            this.materialColumn,
            this.burnRateColumn,
            this.unitsOnPlanetColumn,
            this.daysRemainingColumn});
            this.baseOverviewTreeListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.baseOverviewTreeListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.baseOverviewTreeListView.HideSelection = false;
            this.baseOverviewTreeListView.Location = new System.Drawing.Point(3, 93);
            this.baseOverviewTreeListView.Name = "baseOverviewTreeListView";
            this.baseOverviewTreeListView.ShowGroups = false;
            this.baseOverviewTreeListView.Size = new System.Drawing.Size(794, 359);
            this.baseOverviewTreeListView.TabIndex = 0;
            this.baseOverviewTreeListView.UseAlternatingBackColors = true;
            this.baseOverviewTreeListView.UseCellFormatEvents = true;
            this.baseOverviewTreeListView.UseCompatibleStateImageBehavior = false;
            this.baseOverviewTreeListView.View = System.Windows.Forms.View.Details;
            this.baseOverviewTreeListView.VirtualMode = true;
            this.baseOverviewTreeListView.FormatCell += new System.EventHandler<BrightIdeasSoftware.FormatCellEventArgs>(this.baseOverviewTreeListView_FormatCell);
            // 
            // planetColumn
            // 
            this.planetColumn.AspectName = "PlanetDisplayName";
            this.planetColumn.Text = "Planet";
            this.planetColumn.Width = 131;
            // 
            // materialColumn
            // 
            this.materialColumn.AspectName = "Material";
            this.materialColumn.Text = "Material";
            this.materialColumn.Width = 91;
            // 
            // burnRateColumn
            // 
            this.burnRateColumn.AspectName = "BurnRate";
            this.burnRateColumn.Text = "Burn Rate";
            this.burnRateColumn.Width = 68;
            // 
            // unitsOnPlanetColumn
            // 
            this.unitsOnPlanetColumn.AspectName = "UnitsOnPlanet";
            this.unitsOnPlanetColumn.Text = "Units On Planet";
            this.unitsOnPlanetColumn.Width = 92;
            // 
            // daysRemainingColumn
            // 
            this.daysRemainingColumn.AspectName = "DaysRemainingDisplay";
            this.daysRemainingColumn.Text = "Days Remaining";
            this.daysRemainingColumn.Width = 95;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.baseOverviewTreeListView, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 450);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AutoSize = true;
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel4, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel3, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(794, 84);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.label1);
            this.flowLayoutPanel2.Controls.Add(this.yellowThresholdNumericUpDown);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(114, 78);
            this.flowLayoutPanel2.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Yellow Threshold:";
            // 
            // yellowThresholdNumericUpDown
            // 
            this.yellowThresholdNumericUpDown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.yellowThresholdNumericUpDown.Location = new System.Drawing.Point(3, 36);
            this.yellowThresholdNumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.yellowThresholdNumericUpDown.Name = "yellowThresholdNumericUpDown";
            this.yellowThresholdNumericUpDown.Size = new System.Drawing.Size(91, 20);
            this.yellowThresholdNumericUpDown.TabIndex = 6;
            this.yellowThresholdNumericUpDown.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.yellowThresholdNumericUpDown.ValueChanged += new System.EventHandler(this.yellowThresholdNumericUpDown_ValueChanged);
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.consumablesToShowCheckedListBox);
            this.flowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(243, 3);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(562, 78);
            this.flowLayoutPanel4.TabIndex = 2;
            // 
            // consumablesToShowCheckedListBox
            // 
            this.consumablesToShowCheckedListBox.CheckOnClick = true;
            this.consumablesToShowCheckedListBox.FormattingEnabled = true;
            this.consumablesToShowCheckedListBox.Location = new System.Drawing.Point(3, 3);
            this.consumablesToShowCheckedListBox.Name = "consumablesToShowCheckedListBox";
            this.consumablesToShowCheckedListBox.Size = new System.Drawing.Size(215, 64);
            this.consumablesToShowCheckedListBox.TabIndex = 8;
            this.consumablesToShowCheckedListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.consumablesToShowCheckedListBox_ItemCheck);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.label2);
            this.flowLayoutPanel3.Controls.Add(this.redThresholdNumericUpDown);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(123, 3);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(114, 78);
            this.flowLayoutPanel3.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 10);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 10, 3, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Red Threshold:";
            // 
            // redThresholdNumericUpDown
            // 
            this.redThresholdNumericUpDown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.redThresholdNumericUpDown.Location = new System.Drawing.Point(3, 36);
            this.redThresholdNumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.redThresholdNumericUpDown.Name = "redThresholdNumericUpDown";
            this.redThresholdNumericUpDown.Size = new System.Drawing.Size(80, 20);
            this.redThresholdNumericUpDown.TabIndex = 7;
            this.redThresholdNumericUpDown.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.redThresholdNumericUpDown.ValueChanged += new System.EventHandler(this.redThresholdNumericUpDown_ValueChanged);
            // 
            // BurnRateTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BurnRateTab";
            this.Text = "Burn Rate";
            this.Load += new System.EventHandler(this.WorkforceTab_Load);
            ((System.ComponentModel.ISupportInitialize)(this.baseOverviewTreeListView)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.yellowThresholdNumericUpDown)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.redThresholdNumericUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.TreeListView baseOverviewTreeListView;
        private BrightIdeasSoftware.OLVColumn planetColumn;
        private BrightIdeasSoftware.OLVColumn materialColumn;
        private BrightIdeasSoftware.OLVColumn burnRateColumn;
        private BrightIdeasSoftware.OLVColumn unitsOnPlanetColumn;
        private BrightIdeasSoftware.OLVColumn daysRemainingColumn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown yellowThresholdNumericUpDown;
        private System.Windows.Forms.NumericUpDown redThresholdNumericUpDown;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.CheckedListBox consumablesToShowCheckedListBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    }
}