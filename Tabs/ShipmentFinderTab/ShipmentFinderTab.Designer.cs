﻿namespace FIOUI.Tabs.ShipmentFinderTab
{
    partial class ShipmentFinderTab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShipmentFinderTab));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.shipmentFinderObjectListView = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.payoutColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.payoutPer400UnitsColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.cargoWeightColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.cargoVolumeColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.deliveryTimeColumn = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn9 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn10 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.planetTextBox = new System.Windows.Forms.TextBox();
            this.searchButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.originRadioButton = new System.Windows.Forms.RadioButton();
            this.destinationRadioButton = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.shipmentFinderObjectListView)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.shipmentFinderObjectListView, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(800, 450);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // shipmentFinderObjectListView
            // 
            this.shipmentFinderObjectListView.AllColumns.Add(this.olvColumn1);
            this.shipmentFinderObjectListView.AllColumns.Add(this.olvColumn2);
            this.shipmentFinderObjectListView.AllColumns.Add(this.olvColumn3);
            this.shipmentFinderObjectListView.AllColumns.Add(this.payoutColumn);
            this.shipmentFinderObjectListView.AllColumns.Add(this.payoutPer400UnitsColumn);
            this.shipmentFinderObjectListView.AllColumns.Add(this.cargoWeightColumn);
            this.shipmentFinderObjectListView.AllColumns.Add(this.cargoVolumeColumn);
            this.shipmentFinderObjectListView.AllColumns.Add(this.deliveryTimeColumn);
            this.shipmentFinderObjectListView.AllColumns.Add(this.olvColumn9);
            this.shipmentFinderObjectListView.AllColumns.Add(this.olvColumn10);
            this.shipmentFinderObjectListView.AllowColumnReorder = true;
            this.shipmentFinderObjectListView.AlternateRowBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.shipmentFinderObjectListView.CellEditUseWholeCell = false;
            this.shipmentFinderObjectListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1,
            this.olvColumn2,
            this.olvColumn3,
            this.payoutColumn,
            this.payoutPer400UnitsColumn,
            this.cargoWeightColumn,
            this.cargoVolumeColumn,
            this.deliveryTimeColumn,
            this.olvColumn9,
            this.olvColumn10});
            this.shipmentFinderObjectListView.Cursor = System.Windows.Forms.Cursors.Default;
            this.shipmentFinderObjectListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.shipmentFinderObjectListView.HideSelection = false;
            this.shipmentFinderObjectListView.Location = new System.Drawing.Point(3, 78);
            this.shipmentFinderObjectListView.Name = "shipmentFinderObjectListView";
            this.shipmentFinderObjectListView.ShowGroups = false;
            this.shipmentFinderObjectListView.Size = new System.Drawing.Size(794, 369);
            this.shipmentFinderObjectListView.TabIndex = 0;
            this.shipmentFinderObjectListView.UseAlternatingBackColors = true;
            this.shipmentFinderObjectListView.UseCompatibleStateImageBehavior = false;
            this.shipmentFinderObjectListView.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "ContractPlanetDisplayName";
            this.olvColumn1.Text = "Contract Planet";
            this.olvColumn1.Width = 125;
            // 
            // olvColumn2
            // 
            this.olvColumn2.AspectName = "OriginPlanetDisplayName";
            this.olvColumn2.Text = "Origin Planet";
            this.olvColumn2.Width = 125;
            // 
            // olvColumn3
            // 
            this.olvColumn3.AspectName = "DestinationPlanetDisplayName";
            this.olvColumn3.Text = "Destination Planet";
            this.olvColumn3.Width = 125;
            // 
            // payoutColumn
            // 
            this.payoutColumn.AspectName = "PayoutDisplay";
            this.payoutColumn.Text = "Payout";
            this.payoutColumn.Width = 120;
            // 
            // payoutPer400UnitsColumn
            // 
            this.payoutPer400UnitsColumn.AspectName = "PayoutPricePer400UnitsDisplay";
            this.payoutPer400UnitsColumn.Text = "Payout/400 Units";
            this.payoutPer400UnitsColumn.Width = 100;
            // 
            // cargoWeightColumn
            // 
            this.cargoWeightColumn.AspectName = "CargoWeightDisplay";
            this.cargoWeightColumn.AspectToStringFormat = "";
            this.cargoWeightColumn.Text = "Weight";
            // 
            // cargoVolumeColumn
            // 
            this.cargoVolumeColumn.AspectName = "CargoVolumeDisplay";
            this.cargoVolumeColumn.Text = "Volume";
            // 
            // deliveryTimeColumn
            // 
            this.deliveryTimeColumn.AspectName = "DeliveryTimeDisplay";
            this.deliveryTimeColumn.Text = "Time";
            // 
            // olvColumn9
            // 
            this.olvColumn9.AspectName = "RatingDisplay";
            this.olvColumn9.Text = "Rating";
            // 
            // olvColumn10
            // 
            this.olvColumn10.AspectName = "CreatorCompanyCode";
            this.olvColumn10.Text = "Company";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.planetTextBox, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.searchButton, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(794, 69);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Planet:";
            // 
            // planetTextBox
            // 
            this.planetTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.planetTextBox.Location = new System.Drawing.Point(83, 41);
            this.planetTextBox.Name = "planetTextBox";
            this.planetTextBox.Size = new System.Drawing.Size(194, 20);
            this.planetTextBox.TabIndex = 3;
            // 
            // searchButton
            // 
            this.searchButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.searchButton.Location = new System.Drawing.Point(716, 40);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(75, 23);
            this.searchButton.TabIndex = 4;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.originRadioButton, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.destinationRadioButton, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(83, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(194, 28);
            this.tableLayoutPanel3.TabIndex = 5;
            // 
            // originRadioButton
            // 
            this.originRadioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.originRadioButton.AutoSize = true;
            this.originRadioButton.Location = new System.Drawing.Point(3, 3);
            this.originRadioButton.Name = "originRadioButton";
            this.originRadioButton.Size = new System.Drawing.Size(91, 22);
            this.originRadioButton.TabIndex = 0;
            this.originRadioButton.TabStop = true;
            this.originRadioButton.Text = "Origin";
            this.originRadioButton.UseVisualStyleBackColor = true;
            // 
            // destinationRadioButton
            // 
            this.destinationRadioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.destinationRadioButton.AutoSize = true;
            this.destinationRadioButton.Checked = true;
            this.destinationRadioButton.Location = new System.Drawing.Point(100, 3);
            this.destinationRadioButton.Name = "destinationRadioButton";
            this.destinationRadioButton.Size = new System.Drawing.Size(91, 22);
            this.destinationRadioButton.TabIndex = 1;
            this.destinationRadioButton.TabStop = true;
            this.destinationRadioButton.Text = "Destination";
            this.destinationRadioButton.UseVisualStyleBackColor = true;
            // 
            // ShipmentFinderTab
            // 
            this.AcceptButton = this.searchButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ShipmentFinderTab";
            this.Text = "Shipment Finder";
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.shipmentFinderObjectListView)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private BrightIdeasSoftware.ObjectListView shipmentFinderObjectListView;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox planetTextBox;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.RadioButton originRadioButton;
        private System.Windows.Forms.RadioButton destinationRadioButton;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private BrightIdeasSoftware.OLVColumn olvColumn2;
        private BrightIdeasSoftware.OLVColumn olvColumn3;
        private BrightIdeasSoftware.OLVColumn payoutColumn;
        private BrightIdeasSoftware.OLVColumn cargoWeightColumn;
        private BrightIdeasSoftware.OLVColumn cargoVolumeColumn;
        private BrightIdeasSoftware.OLVColumn deliveryTimeColumn;
        private BrightIdeasSoftware.OLVColumn olvColumn9;
        private BrightIdeasSoftware.OLVColumn olvColumn10;
        private BrightIdeasSoftware.OLVColumn payoutPer400UnitsColumn;
    }
}