﻿using System.Collections;
using System.Windows.Forms;

using BrightIdeasSoftware;

namespace FIOUI.Tabs.ShipmentFinderTab
{
    internal enum ShipmentFinderColumnName
    {
        ContractPlanet = 0,
        OriginPlanet,
        DestinationPlanet,
        PayoutPrice,
        PayoutPricePerUnit,
        Weight,
        Volume,
        DeliveryTime,
        Rating,
        Company
    }

    internal class ShipmentModelSorter : IComparer
    {
        SortOrder _Order;
        ShipmentFinderColumnName _ColumnName;

        public ShipmentModelSorter(SortOrder order, ShipmentFinderColumnName colName)
        {
            _Order = order;
            _ColumnName = colName;
        }

        public int Compare(object x, object y)
        {
            if (_Order != SortOrder.None)
            {
                double a = -1.0;
                double b = -1.0;

                string aStr = null;
                string bStr = null;

                ShipmentModel aObj = ((ShipmentModel)(((OLVListItem)x).RowObject));
                ShipmentModel bObj = ((ShipmentModel)(((OLVListItem)y).RowObject));


                switch (_ColumnName)
                {
                    case ShipmentFinderColumnName.ContractPlanet:
                        aStr = aObj.ContractPlanetDisplayName;
                        bStr = bObj.ContractPlanetDisplayName;
                        break;

                    case ShipmentFinderColumnName.OriginPlanet:
                        aStr = aObj.OriginPlanetDisplayName;
                        bStr = bObj.OriginPlanetDisplayName;
                        break;

                    case ShipmentFinderColumnName.DestinationPlanet:
                        aStr = aObj.DestinationPlanetDisplayName;
                        bStr = bObj.DestinationPlanetDisplayName;
                        break;

                    case ShipmentFinderColumnName.Rating:
                        aStr = aObj.RatingDisplay;
                        bStr = bObj.RatingDisplay;
                        break;

                    case ShipmentFinderColumnName.Company:
                        aStr = aObj.CreatorCompanyCode;
                        bStr = bObj.CreatorCompanyCode;
                        break;

                    case ShipmentFinderColumnName.PayoutPricePerUnit:
                        a = ((ShipmentModel)(((OLVListItem)x).RowObject)).PayoutPricePer400;
                        b = ((ShipmentModel)(((OLVListItem)y).RowObject)).PayoutPricePer400;
                        break;

                    case ShipmentFinderColumnName.PayoutPrice:
                        a = ((ShipmentModel)(((OLVListItem)x).RowObject)).PayoutPrice;
                        b = ((ShipmentModel)(((OLVListItem)y).RowObject)).PayoutPrice;
                        break;

                    case ShipmentFinderColumnName.Weight:
                        a = ((ShipmentModel)(((OLVListItem)x).RowObject)).CargoWeight;
                        b = ((ShipmentModel)(((OLVListItem)y).RowObject)).CargoWeight;
                        break;

                    case ShipmentFinderColumnName.Volume:
                        a = ((ShipmentModel)(((OLVListItem)x).RowObject)).CargoVolume;
                        b = ((ShipmentModel)(((OLVListItem)y).RowObject)).CargoVolume;
                        break;

                    case ShipmentFinderColumnName.DeliveryTime:
                        a = ((ShipmentModel)(((OLVListItem)x).RowObject)).DeliveryTime;
                        b = ((ShipmentModel)(((OLVListItem)y).RowObject)).DeliveryTime;
                        break;
                }

                if (aStr != null && bStr != null)
                {
                    return (_Order == SortOrder.Ascending) ? aStr.CompareTo(bStr) : bStr.CompareTo(aStr);
                }

                if (a != -1.0 && b != -1.0)
                {
                    if (_Order == SortOrder.Ascending)
                    {
                        if (a < b)
                        {
                            return -1;
                        }
                        else if (a > b)
                        {
                            return 1;
                        }
                    }
                    else if (_Order == SortOrder.Descending)
                    {
                        if (a < b)
                        {
                            return 1;
                        }
                        else if (a > b)
                        {
                            return -1;
                        }
                    }
                }
            }

            return 0;
        }
    }
}
