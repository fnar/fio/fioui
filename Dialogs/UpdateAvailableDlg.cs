﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Windows.Forms;

using FIOUI.Web;

namespace FIOUI.Dialogs
{
    public partial class UpdateAvailableDlg : Form
    {
        public UpdateAvailableDlg()
        {
            InitializeComponent();
        }

        private async void UpdateAvailableDlg_Load(object sender, EventArgs e)
        {
            Text = "Release Notes";
            updateButton.Enabled = false;

            releaseNotesRichTextBox.Text = "Retrieving latest version number...";

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string currentVersion = fvi.FileVersion;

            string latestVersion = null;
            using (GetRequest getVersion = new GetRequest("/version/latest"))
            {
                latestVersion = await getVersion.GetResultAsStringAsync();
            }
                
            if (latestVersion != null)
            {
                latestVersion = latestVersion.Trim();

                if (latestVersion != currentVersion || !File.Exists(FilePathConsts.CachedReleaseNotes))
                {
                    releaseNotesRichTextBox.Text = "Downloading Release Notes...";
                    await new WebClient().DownloadFileTaskAsync($"{WebConsts.RootUrl}/version/releasenotes", FilePathConsts.CachedReleaseNotes);
                }

                if (File.Exists(FilePathConsts.CachedReleaseNotes))
                {
                    try
                    {
                        releaseNotesRichTextBox.LoadFile(FilePathConsts.CachedReleaseNotes);
                    }
                    catch
                    {
                        releaseNotesRichTextBox.Text = "ReleaseNotes appear to be corrupted.";
                    }
                    
                }
                else
                {
                    releaseNotesRichTextBox.Text = "Unable to load ReleaseNotes.";
                }
            }

            if (latestVersion != currentVersion)
            {
                updateButton.Enabled = true;
                Text = "Update Available";
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private async void updateButton_Click(object sender, EventArgs e)
        {
            cancelButton.Enabled = false;
            updateButton.Enabled = false;

            try
            {
                releaseNotesRichTextBox.Text = "Downloading...";
                await new WebClient().DownloadFileTaskAsync($"{WebConsts.RootUrl}/version/download", FilePathConsts.InstallerPath);
                Process.Start(FilePathConsts.InstallerPath);
                Application.Exit();
            }
            catch
            {
                MessageBox.Show($"Encountered an unknown error while updating.  Please manually install by visiting:\r\n{WebConsts.RootUrl}/version/download");
                cancelButton.Enabled = true;
                updateButton.Enabled = true;
            }
        }
    }
}
