﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using FIOUI.JsonRepresentations;
using FIOUI.Web;

using Newtonsoft.Json;

namespace FIOUI.Dialogs
{
    public partial class ChangePasswordDlg : Form
    {
        public ChangePasswordDlg()
        {
            InitializeComponent();
        }

        private async void changeButton_Click(object sender, EventArgs e)
        {
            oldPasswordTextBox.Enabled = false;
            newPasswordTextBox1.Enabled = false;
            newPasswordTextBox2.Enabled = false;
            changeButton.Enabled = false;

            if (newPasswordTextBox1.Text == newPasswordTextBox2.Text)
            {
                JsonChangePasswordPayload changePwPayload = new JsonChangePasswordPayload();
                changePwPayload.OldPassword = oldPasswordTextBox.Text;
                changePwPayload.NewPassword = newPasswordTextBox1.Text;

                PostRequest changePwRequest = new PostRequest("/auth/changepassword", Main.Auth.AuthToken, JsonConvert.SerializeObject(changePwPayload));
                await changePwRequest.GetResultNoResponseAsync();
                if ( changePwRequest.StatusCode == HttpStatusCode.OK)
                {
                    MessageBox.Show("Password changed");
                    Close();
                }
                else
                {
                    MessageBox.Show("Password failed to change.  Was the old password correctly typed?");
                }
            }
            else
            {
                MessageBox.Show("Passwords new password and confirmation password do not match.");
            }

            oldPasswordTextBox.Enabled = true;
            newPasswordTextBox1.Enabled = true;
            newPasswordTextBox2.Enabled = true;
            changeButton.Enabled = true;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
