﻿using FIOUI.Web;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using FIOUI.JsonRepresentations;
using Newtonsoft.Json;

namespace FIOUI.Dialogs
{
    public partial class CreateAccountDlg : Form
    {
        public CreateAccountDlg()
        {
            InitializeComponent();
        }

        // Not cryptographically secure, but who cares.
        private static Random rnd = new Random();
        private static string GenerateRandomPassword(int length)
        {
            const string validCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*";
            StringBuilder res = new StringBuilder();
            while (0 < length--)
            {
                res.Append(validCharacters[rnd.Next(validCharacters.Length)]);
            }

            return res.ToString();
        }

        private void CreateAccountDlg_Load(object sender, EventArgs e)
        {
            passwordTextBox.Text = GenerateRandomPassword(16);
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private async void createButton_Click(object sender, EventArgs e)
        {
            createButton.Enabled = false;
            userNameTextBox.Enabled = false;
            passwordTextBox.Enabled = false;
            isAdminCheckBox.Enabled = false;

            if (userNameTextBox.TextLength > 0 && passwordTextBox.TextLength > 5)
            {
                JsonCreateUserPayload createUserPayload = new JsonCreateUserPayload();
                createUserPayload.UserName = userNameTextBox.Text;
                createUserPayload.Password = passwordTextBox.Text;
                createUserPayload.IsAdmin = isAdminCheckBox.Checked;

                using (GetRequest doesUserExistReq = new GetRequest($"/admin/{createUserPayload.UserName}", Main.Auth.AuthToken))
                {
                    await doesUserExistReq.GetResultNoResponseAsync();
                    if (doesUserExistReq.StatusCode == HttpStatusCode.OK)
                    {
                        if (DialogResult.OK != MessageBox.Show("This user already exists.  Replace their password?", "User Already Exists", MessageBoxButtons.OKCancel))
                        {
                            return;
                        }
                    }
                }

                PostRequest createAccountReq = new PostRequest($"/admin/create", Main.Auth.AuthToken, JsonConvert.SerializeObject(createUserPayload));
                await createAccountReq.GetResultNoResponseAsync();
                if ( createAccountReq.StatusCode == HttpStatusCode.OK)
                {
                    Utils.SetClipboardText($"Account information:\r\n```\r\nUserName: {createUserPayload.UserName}\r\nPassword: {createUserPayload.Password}\r\n```");
                    MessageBox.Show("Account created.  Details in clipboard.");
                    Close();
                }
                else
                {
                    MessageBox.Show("Failed to create account.");
                }
            }
            else
            {
                MessageBox.Show("UserName is empty or password is shorter than 6 characters.");
            }

            createButton.Enabled = true;
            userNameTextBox.Enabled = true;
            passwordTextBox.Enabled = true;
            isAdminCheckBox.Enabled = true;
        }
    }
}
