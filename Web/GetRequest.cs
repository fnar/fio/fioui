﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FIOUI.Web
{
    public class GetRequest : IDisposable
    {
        public HttpWebRequest WebReq
        {
            get; private set;
        }

        public HttpStatusCode StatusCode
        {
            get; private set;
        } = HttpStatusCode.ServiceUnavailable;

        public HttpWebResponse WebResp
        {
            get; private set;
        } = null;

        public GetRequest(string EndPoint, string AuthToken = null)
        {
            WebReq = (HttpWebRequest)WebRequest.Create(WebConsts.RootUrl + EndPoint);
            WebReq.KeepAlive = false;
            WebReq.Method = "GET";
            WebReq.Timeout = 20000;
            WebReq.ContentType = "application/json";
            if ( AuthToken != null)
            {
                WebReq.Headers.Add("Authorization", AuthToken);
            }
        }

        public async Task<JsonRepr> GetResponseAsync<JsonRepr>()
        {
           return await Task.Run(() =>
           {
               return GetResponse<JsonRepr>();
           });
        }

        private JsonRepr GetResponse<JsonRepr>()
        {
            try
            {
                WebResp = (HttpWebResponse)WebReq.GetResponse();
                using(Stream s = WebResp.GetResponseStream())
                {
                    StatusCode = WebResp.StatusCode;
                    if ( StatusCode == HttpStatusCode.OK)
                    {
                        using (StreamReader sr = new StreamReader(s))
                        {
                            string payload = sr.ReadToEnd();
                            try
                            {
                                return JsonConvert.DeserializeObject<JsonRepr>(payload);
                            }
                            catch
                            {
                                return default;
                            }
                        }
                    }
                    else
                    {
                        return default;
                    }
                }
            }
            catch(WebException)
            {
                return default;
            }
        }

        public async Task<string> GetResultAsStringAsync()
        {
            return await Task.Run(() => GetResultAsString());
        }

        private string GetResultAsString()
        {
            WebReq.ContentLength = 0;

            WebResp = (HttpWebResponse)WebReq.GetResponse();
            StatusCode = WebResp.StatusCode;
            using (Stream s = WebResp.GetResponseStream())
            {
                if (StatusCode == HttpStatusCode.OK)
                {
                    using (StreamReader sr = new StreamReader(s))
                    {
                        return sr.ReadToEnd();
                    }
                }
                else
                {
                    return null;
                }
            }
        }

        public async Task GetResultNoResponseAsync()
        {
            await Task.Run(() => GetResultNoResponse());
        }

        private void GetResultNoResponse()
        {
            try
            {
                WebReq.Accept = "application/json";
                WebReq.ContentLength = 0;

                WebResp = (HttpWebResponse)WebReq.GetResponse();
                StatusCode = WebResp.StatusCode;
            }
            catch
            {

            }
        }

        private bool _disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                // Dispose managed state (managed objects).
                WebResp?.Close();
                WebResp = null;
            }

            _disposed = true;
        }
    }
}
