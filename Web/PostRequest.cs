﻿using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FIOUI.Web
{
    public class PostRequest
    {
        private string Payload;

        public HttpWebRequest WebReq
        {
            get; private set;
        }

        public HttpStatusCode StatusCode
        {
            get; private set;
        } = HttpStatusCode.ServiceUnavailable;

        public PostRequest(string EndPoint, string AuthToken, string Payload)
        {
            WebReq = (HttpWebRequest)WebRequest.Create(WebConsts.RootUrl + EndPoint);
            WebReq.KeepAlive = false;
            WebReq.Method = "POST";
            WebReq.Timeout = 20000;
            WebReq.ContentType = "application/json";
            if ( AuthToken != null )
            {
                WebReq.Headers.Add("Authorization", AuthToken);
            }
            this.Payload = Payload;
        }

        private bool WritePayloadToRequestStream()
        {
            if(Payload == null)
            {
                WebReq.Accept = "application/json";
                WebReq.ContentLength = 0;
                return true;
            }

            try
            {
                byte[] bytePayload = Encoding.UTF8.GetBytes(Payload);
                WebReq.ContentType = "application/json; charset=UTF-8";
                WebReq.Accept = "application/json";
                WebReq.ContentLength = bytePayload.Length;
                using (Stream requestStream = WebReq.GetRequestStream())
                {
                    requestStream.Write(bytePayload, 0, bytePayload.Length);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public async Task GetResultNoResponseAsync()
        {
            await Task.Run(() =>
            {
                GetResultNoResponse();
            });
        }

        private void GetResultNoResponse()
        {
            try
            {
                if( WritePayloadToRequestStream() )
                {
                    using (HttpWebResponse response = (HttpWebResponse)WebReq.GetResponse())
                    {
                        StatusCode = response.StatusCode;
                    }
                }
            }
            catch
            {

            }
        }

        public async Task<JsonRepr> GetResponseAsync<JsonRepr>()
        {
            return await Task.Run(() =>
            {
                return GetResponse<JsonRepr>();
            });
        }

        private JsonRepr GetResponse<JsonRepr>()
        {
            try
            {
                WritePayloadToRequestStream();

                using (HttpWebResponse response = (HttpWebResponse)WebReq.GetResponse())
                using (Stream s = response.GetResponseStream())
                {
                    StatusCode = response.StatusCode;
                    using (StreamReader sr = new StreamReader(s))
                    {
                        string responsePayload = sr.ReadToEnd();
                        try
                        {
                            return JsonConvert.DeserializeObject<JsonRepr>(responsePayload);
                        }
                        catch
                        {
                            return default;
                        }
                    }
                }
            }
            catch (WebException)
            {
                return default;
            }
        }
    }
}
