﻿using System;
using System.IO;

namespace FIOUI
{
    public static class FilePathConsts
    {
        public static string AppDataDirectory
        {
            get
            {
                string BaseDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "FIOUI");
                if (!Directory.Exists(BaseDir))
                {
                    Directory.CreateDirectory(BaseDir);
                }

                return BaseDir;
            }
        }

        public static string MainWindowSettings
        {
            get
            {
                return Path.Combine(AppDataDirectory, "appsettings.json");
            }
        }

        public static string DockPanelSettings
        {
            get
            {
                return Path.Combine(AppDataDirectory, "dockpanelsettings.xml");
            }
        }

        public static string CachedReleaseNotes
        {
            get
            {
                return Path.Combine(AppDataDirectory, "ReleaseNotes.rtf");
            }
        }

        public static string InstallerPath
        {
            get
            {
                return Path.Combine(AppDataDirectory, "FIOUI-Setup.exe");
            }
        }

        public static string DataCachePath
        {
            get
            {
                string DataCacheDir = Path.Combine(AppDataDirectory, "DataCache");
                if ( !Directory.Exists(DataCacheDir))
                {
                    Directory.CreateDirectory(DataCacheDir);
                }

                return DataCacheDir;
            }
        }
    }
}
