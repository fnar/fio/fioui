﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

using FIOUI.Tabs;

using Newtonsoft.Json;

using WeifenLuo.WinFormsUI.Docking;

namespace FIOUI
{
    public static class Settings
    {
        internal class OLVSetting
        {
            public string TabName { get; set; }
            public string DataUserName { get; set; }

            public List<byte[]> OLVStates { get; set; }
            public string GeneralState { get; set; }
        }

        internal class MainWindowSettings
        {
            public string UserName { get; set; } = "";

            public int MainWindowX { get; set; } = 50;
            public int MainWindowY { get; set; } = 50;
            public int MainWindowWidth { get; set; } = 900;
            public int MainWindowHeight { get; set; } = 500;
            public bool MainWindowMaximized { get; set; } = false;

            public List<OLVSetting> OLVSettings { get; set; } = new List<OLVSetting>();
        }

        private static MainWindowSettings MainSettings = new MainWindowSettings();

        public static string UserName
        {
            get
            {
                return MainSettings.UserName;
            }
        }

        #region Main Form SaveSettings/RestoreSettings
        public static void SaveSettings(this Main mainForm)
        {
            MainSettings.UserName = Main.Auth.UserName;
            if ( mainForm.WindowState == FormWindowState.Maximized)
            {
                MainSettings.MainWindowX = mainForm.RestoreBounds.X;
                MainSettings.MainWindowY = mainForm.RestoreBounds.Y;
                MainSettings.MainWindowWidth = mainForm.RestoreBounds.Width;
                MainSettings.MainWindowHeight = mainForm.RestoreBounds.Height;
                MainSettings.MainWindowMaximized = true;
            }
            else
            {
                MainSettings.MainWindowX = mainForm.Location.X;
                MainSettings.MainWindowY = mainForm.Location.Y;
                MainSettings.MainWindowWidth = mainForm.Size.Width;
                MainSettings.MainWindowHeight = mainForm.Size.Height;
                MainSettings.MainWindowMaximized = false;
            }

            // Make sure to clear the old settings...
            MainSettings.OLVSettings.Clear();

            foreach(var tab in FIOTab.OpenFIOTabs)
            {
                OLVSetting setting = new OLVSetting();
                setting.TabName = tab.GetType().FullName;
                setting.DataUserName = tab.DataUserName;
                setting.OLVStates = tab.SaveOLVStates();
                setting.GeneralState = tab.SaveGeneralState();
                MainSettings.OLVSettings.Add(setting);
            }

            try
            {
                string MainSettingsStr = JsonConvert.SerializeObject(MainSettings);
                File.WriteAllText(FilePathConsts.MainWindowSettings, JsonConvert.SerializeObject(MainSettings));
            }
            catch
            {

            }
        }

        public static bool ReadSettings(this Main mainForm)
        {
            try
            {
                if (File.Exists(FilePathConsts.MainWindowSettings))
                {
                    MainSettings = JsonConvert.DeserializeObject<MainWindowSettings>(File.ReadAllText(FilePathConsts.MainWindowSettings));
                    return true;
                }
            }
            catch
            {

            }

            return false;
        }

        public static bool RestoreSettings(this Main mainForm)
        {
            if ( ReadSettings(mainForm) )
            {
                mainForm.Location = new Point(MainSettings.MainWindowX, MainSettings.MainWindowY);
                mainForm.Size = new Size(MainSettings.MainWindowWidth, MainSettings.MainWindowHeight);
                mainForm.WindowState = MainSettings.MainWindowMaximized ? FormWindowState.Maximized : FormWindowState.Normal;


                bool bIsTopLeftOnScreen = false;
                foreach (Screen screen in Screen.AllScreens)
                {
                    Point windowsFormTopLeft = new Point(mainForm.Left, mainForm.Top);
                    if (screen.WorkingArea.Contains(windowsFormTopLeft))
                    {
                        bIsTopLeftOnScreen = true;
                        break;
                    }
                }

                if (!bIsTopLeftOnScreen)
                {
                    // This can happen when changing monitor setups or something of that nature
                    // Reset to sane defaults
                    mainForm.Location = new Point(20, 20);
                    mainForm.Size = new Size(900, 525);
                    mainForm.WindowState = FormWindowState.Normal;
                }

                return true;
            }

            return false;
        }
        #endregion

        #region DockPanel SaveSettings/RestoreSettings
        public static void SaveSettings(this DockPanel dockPanel)
        {
            dockPanel.SaveAsXml(FilePathConsts.DockPanelSettings);
        }

        public static bool RestoreSettings(this DockPanel dockPanel)
        {
            if (File.Exists(FilePathConsts.DockPanelSettings))
            {
                dockPanel.LoadFromXml(FilePathConsts.DockPanelSettings, new DeserializeDockContent(GetContentFromPersistString));
                return true;
            }
            else
            {
                return false;
            }
        }

        private static IDockContent GetContentFromPersistString(string persistString)
        {
            string[] splitRes = persistString.Split(new char[] { ',' });

            string typeName = splitRes[0].Trim();
            string dataUserName = splitRes[1].Trim();

            var SupportedFIOTabTypes = from t in Assembly.GetExecutingAssembly().GetTypes() where t.IsSubclassOf(typeof(FIOTab)) select t;
            var classType = SupportedFIOTabTypes.Where(t => t.FullName == typeName).FirstOrDefault();
            if (classType != null)
            {
                var instance = (FIOTab)Activator.CreateInstance(classType);
                instance.DataUserName = dataUserName;

                var olvsetting = MainSettings.OLVSettings.Where(olvs => olvs.TabName == typeName && olvs.DataUserName == dataUserName).FirstOrDefault();
                if (olvsetting != null)
                {
                    bool result = instance.RestoreOLVStates(olvsetting.OLVStates);
                    System.Diagnostics.Debug.Assert(result, "This can occur when the OLV has changed.");

                    result = instance.RestoreGeneralState(olvsetting.GeneralState);
                    System.Diagnostics.Debug.Assert(result, "This can occur when the JSON format has changed drastically enough.");
                }

                _ = instance.RefreshDataAsync();
                return instance;
            }

            return null;
        }
        #endregion
    }
}
