﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIOUI.JsonRepresentations
{
    public class JsonShips
    {
        public List<Ship> Ships { get; set; } = new List<Ship>();

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class Ship
    {
        public string ShipId { get; set; }
        public string StoreId { get; set; }
        public string StlFuelStoreId { get; set; }
        public string FtlFuelStoreId { get; set; }
        public string Registration { get; set; }
        public string Name { get; set; }
        public string Manufacturer { get; set; }

        public string FlightId { get; set; }
        public double Acceleration { get; set; }
        public double Thrust { get; set; }
        public double Mass { get; set; }
        public double OperatingEmptyMass { get; set; }
        public double ReactorPower { get; set; }
        public double ReactorUtilization { get; set; }
        public double EmitterMinPower { get; set; }
        public double EmitterMaxPower { get; set; }
        public double Volume { get; set; }

        public string Location { get; set; }

        public double StlFuelFlowRate { get; set; }
    }
}
