﻿using System;
using System.Collections.Generic;

namespace FIOUI.JsonRepresentations
{
    public class PlanetData
    {
        public string Id { get; set; }
        public string NaturalId { get; set; }
        public string Name { get; set; }
        public string Namer { get; set; }
        public long NamingDataEpochMs { get; set; }
        public bool Nameable { get; set; }

        public double Gravity { get; set; }
        public double MagneticField { get; set; }
        public double Mass { get; set; }
        public double MassEarth { get; set; }

        public double OrbitSemiMajorAxis { get; set; }
        public double OrbitEccentricity { get; set; }
        public double OrbitInclination { get; set; }
        public double OrbitRightAscension { get; set; }
        public double OrbitPeriapsis { get; set; }
        public int OrbitIndex { get; set; }

        public double Pressure { get; set; }
        public double Radiation { get; set; }
        public double Radius { get; set; }

        public virtual List<Resource> Resources { get; set; } = new List<Resource>();

        public double Sunlight { get; set; }
        public bool Surface { get; set; }
        public double Temperature { get; set; }
        public double Fertility { get; set; }

        public virtual List<BuildRequirement> BuildRequirements { get; set; } = new List<BuildRequirement>();

        public bool HasLocalMarket { get; set; }
        public bool HasChamberOfCommerce { get; set; }
        public bool HasWarehouse { get; set; }
        public bool HasAdministrationCenter { get; set; }

        public string FactionCode { get; set; }
        public string FactionName { get; set; }

        public string GovernorId { get; set; }
        public string GovernorUserName { get; set; }

        public string GovernorCorporationId { get; set; }
        public string GovernorCorporationName { get; set; }
        public string GovernorCorporationCode { get; set; }

        public string CurrencyName { get; set; }
        public string CurrencyCode { get; set; }

        public string CollectorId { get; set; }
        public string CollectorName { get; set; }
        public string CollectorCode { get; set; }

        public virtual List<ProductionFee> ProductionFees { get; set; } = new List<ProductionFee>();
        public double? BaseLocalMarketFee { get; set; }
        public double? LocalMarketFeeFactor { get; set; }

        public double? WarehouseFee { get; set; }

        public string PopulationId { get; set; }

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class Resource
    {
        public string MaterialId { get; set; }
        public string ResourceType { get; set; }
        public double Factor { get; set; }
    }

    public class BuildRequirement
    {
        public string MaterialName { get; set; }
        public string MaterialId { get; set; }
        public string MaterialTicker { get; set; }
        public string MaterialCategory { get; set; }

        public int MaterialAmount { get; set; }

        public double MaterialWeight { get; set; }
        public double MaterialVolume { get; set; }
    }

    public class ProductionFee
    {
        public string Category { get; set; }
        public double FeeAmount { get; set; }
        public string FeeCurrency { get; set; }
    }
}
