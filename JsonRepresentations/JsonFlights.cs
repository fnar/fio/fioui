﻿using System;
using System.Collections.Generic;

namespace FIOUI.JsonRepresentations
{
    public class JsonFlights
    {
        public List<Flight> Flights { get; set; } = new List<Flight>();

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class Flight
    {
        public string FlightId { get; set; }
        public string ShipId { get; set; }

        public string Origin { get; set; }
        public string Destination { get; set; }

        public long DepartureTimeEpochMs { get; set; }
        public long ArrivalTimeEpochMs { get; set; }

        public List<Segment> Segments { get; set; } = new List<Segment>();

        public int CurrentSegmentIndex { get; set; }

        public double StlDistance { get; set; }
        public double FtlDistance { get; set; }

        public bool IsAborted { get; set; }
    }

    public class Segment
    {
        public string Type { get; set; }

        public long DepartureTimeEpochMs { get; set; }
        public long ArrivalTimeEpochMs { get; set; }

        public double? StlDistance { get; set; }
        public double? StlFuelConsumption { get; set; }
        public double? FtlDistance { get; set; }
        public double? FtlFuelConsumption { get; set; }

        public string Origin { get; set; }
        public string Destination { get; set; }

        public List<OriginLine> OriginLines { get; set; } = new List<OriginLine>();
        public List<DestinationLine> DestinationLines { get; set; } = new List<DestinationLine>();
    }

    public class OriginLine
    {
        public string Type { get; set; }
        public string LineId { get; set; }
        public string LineNaturalId { get; set; }
        public string LineName { get; set; }
    }

    public class DestinationLine
    {
        public string Type { get; set; }
        public string LineId { get; set; }
        public string LineNaturalId { get; set; }
        public string LineName { get; set; }

    }
}
