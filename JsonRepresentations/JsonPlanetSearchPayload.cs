﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FIOUI.JsonRepresentations
{
    public class JsonPlanetSearch
    {
        public List<string> Materials { get; set; } = new List<string>();

        public bool IncludeRocky { get; set; }
        public bool IncludeGaseous { get; set; }

        public bool IncludeLowGravity { get; set; }
        public bool IncludeHighGravity { get; set; }

        public bool IncludeLowPressure { get; set; }
        public bool IncludeHighPressure { get; set; }

        public bool IncludeLowTemperature { get; set; }
        public bool IncludeHighTemperature { get; set; }

        public bool MustBeFertile { get; set; }
        public bool MustHaveLocalMarket { get; set; }
        public bool MustHaveChamberOfCommerce { get; set; }
        public bool MustHaveWarehouse { get; set; }
        public bool MustHaveAdministrationCenter { get; set; }
    }
}
