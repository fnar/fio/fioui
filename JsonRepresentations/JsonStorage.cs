﻿using System;

namespace FIOUI.JsonRepresentations.Storage
{
    public class Rootobject
    {
        public Storageitem[] StorageItems { get; set; }
        public string AddressableId { get; set; }
        public object Name { get; set; }
        public float WeightLoad { get; set; }
        public float WeightCapacity { get; set; }
        public float VolumeLoad { get; set; }
        public float VolumeCapacity { get; set; }
        public bool FixedStore { get; set; }
        public string Type { get; set; }
        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class Storageitem
    {
        public string MaterialId { get; set; }
        public string MaterialName { get; set; }
        public string MaterialTicker { get; set; }
        public string MaterialCategory { get; set; }
        public float MaterialWeight { get; set; }
        public float MaterialVolume { get; set; }
        public int MaterialAmount { get; set; }
        public string Type { get; set; }
        public float TotalWeight { get; set; }
        public float TotalVolume { get; set; }
    }

}
