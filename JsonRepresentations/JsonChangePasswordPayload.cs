﻿namespace FIOUI.JsonRepresentations
{
    public class JsonChangePasswordPayload
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
