﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace FIOUI.JsonRepresentations
{
    public class SystemStars
    {
        public string SystemId { get; set; }
        public string Name { get; set; }
        public string NaturalId { get; set; }
        public string Type { get; set; }

        [JsonIgnore]
        public string DisplayName
        {
            get
            {
                if (NaturalId != Name)
                {
                    return $"{Name} ({NaturalId})";
                }
                else
                {
                    return NaturalId;
                }
            }
        }

        public double PositionX { get; set; }
        public double PositionY { get; set; }
        public double PositionZ { get; set; }

        public string SectorId { get; set; }
        public string SubSectorId { get; set; }

        public virtual List<SystemConnection> Connections { get; set; } = new List<SystemConnection>();

        public string UserNameSubmitted { get; set; }
        public DateTime Timestamp { get; set; }
    }

    public class SystemConnection
    {
        public string Connection { get; set; }
    }
}
