﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Windows.Forms;
using System.IO;

using FIOUI.Dialogs;
using FIOUI.Web;

using Newtonsoft.Json;

namespace FIOUI
{
    public class PermissionAllowance
    {
        public string UserName { get; set; }

        public bool FlightData { get; set; }
        public bool BuildingData { get; set; }
        public bool StorageData { get; set; }
        public bool ProductionData { get; set; }
        public bool WorkforceData { get; set; }
        public bool ExpertsData { get; set; }
    }

    public class Authentication
    {
        public bool IsAuthenticated
        {
            get
            {
                return !String.IsNullOrWhiteSpace(AuthToken);
            }
        }

        public string UserName
        {
            get; private set;
        } = null;

        #region AuthToken
        private object AuthTokenLockObj = new object();
        public string AuthToken
        {
            get
            {
                lock(AuthTokenLockObj)
                {
                    if (_AuthToken != null)
                    {
                        return String.Copy(_AuthToken);
                    }

                    return null;
                }
            }
            private set
            {
                lock(AuthTokenLockObj)
                {
                    _AuthToken = value;
                }
            }
        }
        private string _AuthToken = null;
        #endregion

        #region PermissionAllowances
        private object PermissionAllowancesLockObj = new object();
        public List<PermissionAllowance> PermissionAllowances
        {
            get
            {
                lock(PermissionAllowancesLockObj)
                {
                    if ( _PermissionAllowances != null)
                    {
                        return new List<PermissionAllowance>(_PermissionAllowances);
                    }

                    return null;
                }
            }
            private set
            {
                lock(PermissionAllowancesLockObj)
                {
                    _PermissionAllowances = value;
                }
            }
        }
        private List<PermissionAllowance> _PermissionAllowances = null;
        #endregion

        public Authentication( string UserName = "")
        {
            this.UserName = UserName;
        }

        public async Task<bool> PromptLogin()
        {
            LoginDlg loginDlg = new LoginDlg(UserName);
            var result = loginDlg.ShowDialog();
            if (result == DialogResult.Cancel)
            {
                return false;
            }

            return await Login(loginDlg.UserName, loginDlg.Password);
        }

        private async Task<bool> Login(string userName, string password)
        {
            var loginPayload = new JsonRepresentations.JsonLoginPayload();
            loginPayload.UserName = userName;
            loginPayload.Password = password;

            PostRequest loginRequest = new PostRequest("/auth/login", null, JsonConvert.SerializeObject(loginPayload));
            var loginResultPayload = await loginRequest.GetResponseAsync<JsonRepresentations.JsonAuthLoginPayload>();
            if (loginResultPayload != null)
            {
                UserName = loginPayload.UserName;
                AuthToken = loginResultPayload.AuthToken;

                await RefreshPermissions();

                return true;
            }

            return false;
        }

        public async Task<bool> RefreshAuthToken()
        {
            PostRequest refreshReq = new PostRequest("/auth/refreshauthtoken", AuthToken, null);
            await refreshReq.GetResultNoResponseAsync();
            return refreshReq.StatusCode == HttpStatusCode.OK;
        }

        public async Task<bool> RefreshPermissions()
        {
            using (GetRequest permReq = new GetRequest("/auth/visibility", AuthToken))
            {
                PermissionAllowances = await permReq.GetResponseAsync<List<PermissionAllowance>>();
                return PermissionAllowances != null;
            }
        }
    }
}
