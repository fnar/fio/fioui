﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

using FIOUI.Web;

using Newtonsoft.Json;

using Material = FIOUI.JsonRepresentations.Material;
using SystemStars = FIOUI.JsonRepresentations.SystemStars;
using WorldSectors = FIOUI.JsonRepresentations.WorldSectors;

namespace FIOUI
{
    public static class DataCache
    {
        private static List<Material> _BasicConsumables = null;
        private static List<Material> _LuxuryConsumables = null;
        private static List<Material> _Minerals = null;
        private static List<Material> _Ores = null;
        private static List<Material> _Gases = null;
        private static List<Material> _Liquids = null;
        private static List<Material> _Fuels = null;

        private static List<SystemStars> _SystemStars = null;
        private static List<WorldSectors> _WorldSectors = null;

        private static T ReadFromFile<T>(string FilePath)
        {
            if ( !File.Exists(FilePath))
            {
                return default(T);
            }

            try
            {
                return JsonConvert.DeserializeObject<T>(File.ReadAllText(FilePath));
            }
            catch
            {
                return default(T);
            }
        }

        private static void WriteToFile<T>(string FilePath, T obj)
        {
            File.WriteAllText(FilePath, JsonConvert.SerializeObject(obj));
        }

        public static async Task<List<Material>> GetBasicConsumables()
        {
            if (_BasicConsumables == null)
            {
                string CachePath = Path.Combine(FilePathConsts.DataCachePath, "BasicConsumables.json");
                _BasicConsumables = ReadFromFile<List<Material>>(CachePath);
                if (_BasicConsumables == null )
                {
                    using (GetRequest getBasicConsumables = new GetRequest("/material/category/consumables (basic)"))
                    {
                        _BasicConsumables = await getBasicConsumables.GetResponseAsync<List<Material>>();
                        WriteToFile(CachePath, _BasicConsumables);
                    }
                }
            }

            return _BasicConsumables;
        }


        public static async Task<List<Material>> GetLuxuryConsumables()
        {
            if (_LuxuryConsumables == null)
            {
                string CachePath = Path.Combine(FilePathConsts.DataCachePath, "LuxuryConsumables.json");
                _LuxuryConsumables = ReadFromFile<List<Material>>(CachePath);
                if (_LuxuryConsumables == null)
                {
                    using (GetRequest getLuxuryConsumables = new GetRequest("/material/category/consumables (luxury)"))
                    {
                        _LuxuryConsumables = await getLuxuryConsumables.GetResponseAsync<List<Material>>();
                        WriteToFile(CachePath, _LuxuryConsumables);
                    }
                }
            }

            return _LuxuryConsumables;
        }

        public static async Task<List<Material>> GetMinerals()
        {
            if (_Minerals == null)
            {
                string CachePath = Path.Combine(FilePathConsts.DataCachePath, "Minerals.json");
                _Minerals = ReadFromFile<List<Material>>(CachePath);
                if (_Minerals == null)
                {
                    using (GetRequest getMinerals = new GetRequest("/material/category/minerals"))
                    {
                        _Minerals = await getMinerals.GetResponseAsync<List<Material>>();
                        WriteToFile(CachePath, _Minerals);
                    }
                }
            }

            return _Minerals;
        }

        public static async Task<List<Material>> GetOres()
        {
            if (_Ores == null)
            {
                string CachePath = Path.Combine(FilePathConsts.DataCachePath, "Ores.json");
                _Ores = ReadFromFile<List<Material>>(CachePath);
                if (_Ores == null)
                {
                    using (GetRequest getOres = new GetRequest("/material/category/ores"))
                    {
                        _Ores = await getOres.GetResponseAsync<List<Material>>();
                        WriteToFile(CachePath, _Ores);
                    }
                }
            }

            return _Ores;
        }

        public static async Task<List<Material>> GetGasses()
        {
            if (_Gases == null)
            {
                string CachePath = Path.Combine(FilePathConsts.DataCachePath, "Gases.json");
                _Gases = ReadFromFile<List<Material>>(CachePath);
                if (_Gases == null)
                {
                    using (GetRequest getGases = new GetRequest("/material/category/gases"))
                    {
                        _Gases = await getGases.GetResponseAsync<List<Material>>();
                        WriteToFile(CachePath, _Gases);
                    }
                }
            }

            return _Gases;
        }

        public static async Task<List<Material>> GetLiquids()
        {
            if (_Liquids == null)
            {
                string CachePath = Path.Combine(FilePathConsts.DataCachePath, "Liquids.json");
                _Liquids = ReadFromFile<List<Material>>(CachePath);
                if (_Liquids == null)
                {
                    using (GetRequest getLiquids = new GetRequest("/material/category/liquids"))
                    {
                        _Liquids = await getLiquids.GetResponseAsync<List<Material>>();
                        WriteToFile(CachePath, _Liquids);
                    }
                }
            }

            return _Liquids;
        }

        public static async Task<List<Material>> GetFuels()
        {
            if (_Fuels == null)
            {
                string CachePath = Path.Combine(FilePathConsts.DataCachePath, "Fuels.json");
                _Fuels = ReadFromFile<List<Material>>(CachePath);
                if (_Fuels == null)
                {
                    using (GetRequest getFuels = new GetRequest("/material/category/fuels"))
                    {
                        _Fuels = await getFuels.GetResponseAsync<List<Material>>();
                        WriteToFile(CachePath, _Fuels);
                    }
                }
            }

            return _Fuels;
        }

        public static async Task<List<SystemStars>> GetSystemStars()
        {
            if (_SystemStars == null)
            {
                string CachePath = Path.Combine(FilePathConsts.DataCachePath, "SystemStars.json");
                _SystemStars = ReadFromFile<List<SystemStars>>(CachePath);
                if (_SystemStars == null)
                {
                    using (GetRequest getSystemStars = new GetRequest("/systemstars"))
                    {
                        _SystemStars = await getSystemStars.GetResponseAsync<List<SystemStars>>();
                        WriteToFile(CachePath, _SystemStars);
                    }
                }
            }

            return _SystemStars;
        }

        public static async Task<List<WorldSectors>> GetWorldSectors()
        {
            if (_WorldSectors == null)
            {
                string CachePath = Path.Combine(FilePathConsts.DataCachePath, "WorldSectors.json");
                _WorldSectors = ReadFromFile<List<WorldSectors>>(CachePath);
                if (_WorldSectors == null)
                {
                    using (GetRequest getWorldSectors = new GetRequest("/systemstars/worldsectors"))
                    {
                        _WorldSectors = await getWorldSectors.GetResponseAsync<List<WorldSectors>>();
                        WriteToFile(CachePath, _WorldSectors);
                    }
                }
            }

            return _WorldSectors;
        }
    }
}
